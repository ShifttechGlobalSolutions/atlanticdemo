@extends('layouts.portalMaster')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Video Details</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content profile-tab-cont">
                                <div class="table-responsive tab-pane show active" id="all_polices">
                                    @foreach($video as $v)
                                        <div style="text-align:center; margin-left: 6%">
                                            <h3>{{ $v->title }}</h3><br>
                                            <video  controls width="420" height="300" controlsList="nodownload">
                                                <source src="{{ asset('public/storage/video/'.$v->video) }}" type="video/mp4">
                                            </video><br><br>
                                            <p>{{ $v->description }}</p><br>
                                            <a href="" class="btn btn-primary" data-toggle="modal" data-target="#updateVid">Edit Video</a>
                                        </div>
                                    @endforeach
                                </div>
                            </div><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="updateVid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #0C0457;color:white">
                    <h5 class="modal-title" id="exampleModalLabel">Add Update Video</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="background-color: #ffffff">
                    @foreach($video as $vid)
                    <form method="post" action="{{ route('admin/add-video') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="hidden" value="{{ $vid->id }}" class="form-control" id="id" name="id" placeholder="Video Title" style="background-color: #F5F5F5;color: black">
                            <input type="text" value="{{ $vid->title }}" class="form-control" id="title" name="title" placeholder="Video Title" style="background-color: #F5F5F5;color: black">
                        </div>

                        <div class="form-group">
                            <label for="file">Video</label>
                            <input type="file" class="form-control" id="file" name="file" value="{{ $vid->video }}"  style="background-color: #F5F5F5;color: black">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="description" placeholder="Video Description">{{ $vid->description }}</textarea>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endsection
