<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpouseInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spouse_infos', function (Blueprint $table) {
            $table->id();
            $table->string('policy_no');
            $table->string('spouse_firstname');
            $table->string('spouse_lastname');
            $table->datetime('spouse_dob');
            $table->string('spouse_idNumber');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spouse_infos');
    }
}
