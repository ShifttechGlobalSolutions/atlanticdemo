@extends("layouts.master")
@section("content")


    <!-- Main content Start -->

    <div class="home-area home-slides-two owl-carousel owl-theme">
        <div class="banner-section item-bg3">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="main-banner-content">
                            <span class="sub-title">Funeral cover</span>
                            <h2 style="color: ghostwhite">Plan ahead and protect the ones you love.</h2>
                            <p>Choosing funeral cover from Atlantic allows you to take control and protect you and your family from additional expenses and challenges while grieving.</p>
                            <div class="btn-box">
                                <a href="{{url('/applications')}}" class="default-btn">Buy Cover Online <span></span></a>


                                <a href="{{url('/')}}" class="optional-btn"> Call me back <span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
{{--        <div class="banner-section item-bg1">--}}
{{--            <div class="d-table">--}}
{{--                <div class="d-table-cell">--}}
{{--                    <div class="container">--}}
{{--                        <div class="main-banner-content">--}}
{{--                            <span class="sub-title">Funeral cover</span>--}}
{{--                            <h1>--}}
{{--                                Prepaid funeral plans safeguard against the unexpected.</h1>--}}
{{--                            <p>Our funeral cover products provide all the financial and practical help that you and your family will need during a difficult time.</p>--}}
{{--                            <div class="btn-box">--}}
{{--                                <a href="{{url('/applications')}}" class="default-btn">Buy Cover Online <span></span></a>--}}

{{--                                <a href="contact.html" class="optional-btn">Download Brochure <span></span></a>--}}
{{--                                <a href="contact.html" class="optional-btn"> Call me back <span></span></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

    </div>





    <section class="about-area ptb-100 bg-f8f8f8">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="about-title">

                        <h2>Our Funeral Covers</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="about-text">
                        <p>We have 4 funeral products, each with its own unique benefits, which you can choose from to suit your needs: </p>
                        <a href="about.html" class="read-more-btn">More About Us <i class="flaticon-right-chevron"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="services-area bg-f8f8f8 pb-70">
        <div class="container">
            <div class="services-slides owl-carousel owl-theme">
                <div class="single-services-box" style="height: 524px">
                    <div class="icon">
                        <i class="flaticon-home-insurance"></i>
                        <div class="icon-bg">
                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                        </div>
                    </div>
                    <h3><a href="home-insurance.html">Funeral Policies </a></h3>
                    <p>We offer various funeral policies to suit you budget. Our policies are structured to accomodate everyone in need
                        of assurance and would gladly assist you in choosing the right plan for you and your loved ones.</p>
                    <a href="home-insurance.html" class="read-more-btn">Learn More</a>
                    <div class="box-shape">
                        <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                        <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                    </div>
                </div>
                <div class="single-services-box" style="height: 524px">
                    <div class="icon">
                        <i class="flaticon-insurance"></i>
                        <div class="icon-bg">
                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                        </div>
                    </div>
                    <h3><a href="business-insurance.html">Funeral Arrangements</a></h3>
                    <p>We cater for all funeral arrangements for a complete peace of mind.
                        We believe that your loved one should be layed to rest with dignity and compassion.
                        Our experience and expertise puts us up there with the best.
                        Contact us and we will be more than happy to assist you.</p>
                    <a href="home-insurance.html" class="read-more-btn">Learn More</a>
                    <div class="box-shape">
                        <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                        <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                    </div>
                </div>
                <div class="single-services-box" style="height: 524px">
                    <div class="icon">
                        <i class="flaticon-health-insurance"></i>
                        <div class="icon-bg">
                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                        </div>
                    </div>
                    <h3><a href="health-insurance.html">Cash Funerals</a></h3>
                    <p> For the times when death unexpectingly comes into your life.
                        We offer cash funerals to suit your budget. contact us today to discuss in more detail.</p>
                    <a href="home-insurance.html" class="read-more-btn">Learn More</a>
                    <div class="box-shape">
                        <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                        <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                    </div>
                </div>
                <div class="single-services-box" style="height: 524px">
                    <div class="icon">
                        <i class="flaticon-travel-insurance"></i>
                        <div class="icon-bg">
                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                        </div>
                    </div>
                    <h3><a href="travel-insurance.html">Cremations</a></h3>
                    <p>Atlantic Funeral Services offers the option of cremation. When a loved one passes on,
                        we are left with so much decisions to make. Let us assist you in making the right choice.</p>
                    <a href="home-insurance.html" class="read-more-btn">Learn More</a>
                </div>
                <div class="single-services-box" style="height: 524px">
                    <div class="icon">
                        <i class="flaticon-car-insurance"></i>
                        <div class="icon-bg">
                            <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                        </div>
                    </div>
                    <h3><a href="car-insurance.html">Tombstones</a></h3>
                    <p>We offer tombstones in various sizes and designs. We can design it in accordance with how you would like it.
                        There are also various options to choose from depending on your budget.</p>
                    <a href="home-insurance.html" class="read-more-btn">Learn More</a>
                    <div class="box-shape">
                        <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                        <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="why-choose-us-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="why-choose-us-slides owl-carousel owl-theme">
                        <div class="why-choose-us-image bg1">
                            <img src="assets/img/why-choose-img1.jpg" alt="image">
                        </div>
                        <div class="why-choose-us-image bg2">
                            <img src="assets/img/why-choose-img2.jpg" alt="image">
                        </div>
                        <div class="why-choose-us-image bg3">
                            <img src="assets/img/why-choose-img3.jpg" alt="image">
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12">
                    <div class="why-choose-us-content">
                        <div class="content">
                            <div class="title">
                                <span class="sub-title">Your Benefits</span>
                                <h2>Why Choose Us</h2>
                                <p> We Can Guide You Through The Process.</p>
                            </div>
                            <ul class="features-list">
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-like"></i>
                                    </div>
                                    <span>25 Years of experience</span>
{{--                                    We have cultivated an understanding of the funeral insurance needs for South Africans from all walks of life.--}}

                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-customer-service"></i>
                                    </div>
                                    <span>Valuable products</span>
{{--                                    We have cultivated an understanding of the funeral insurance needs for South Africans from all walks of life.--}}

                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-care"></i>
                                    </div>
                                    <span>Credibility</span>
{{--                                    We have been providing South Africans with affordable insurance products for over 25 years--}}
                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-team"></i>
                                    </div>
                                    <span>Clients Focused</span>
{{--                                    Our service centres assures you of friendly and efficient service and assistance with your insurance needs.--}}
                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-policy"></i>
                                    </div>
                                    <span>Reputation</span>
{{--                                    We are a respected and trusted name in insurance.--}}
                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-education"></i>
                                    </div>
                                    <span>Simplicity</span>
{{--                                    We keep things as simple and easy as possible and make buying insurance clear and simple.--}}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="quote-area ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="quote-content">
                        <h2>Get a free quote</h2>
                        <div class="image">
                            <img src="{{asset('public/frontend/img/img1.png')}}" alt="image">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="tab quote-list-tab">
                        <ul class="tabs">
                            <li><a href="index.html">CALL ME BACK</a></li>
                            <li><a href="#">ONLINE QUOTE</a></li>


                        </ul>
                        <div class="tab_content">
                            <div class="tabs_item">
                                <h3>CALL ME BACK</h3>
                                <p>Complete the form below and one of our friendly consultants will call you back.</p>
                                <form>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Your Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Your Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Your Phone">
                                    </div>

                                    <button type="submit" class="default-btn">CALL ME BACK <span></span></button>
                                </form>
                            </div>
                            <div class="tabs_item">
                                <h3>ONLINE QUOTE</h3>
                                <p>Complete the form below to begin with the online quote.</p>
                                <form>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Your Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Your Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Your Phone">
                                    </div>
                                    <div class="form-group">

                                    </div>
                                    <button type="submit" class="default-btn">CALL ME BACK <span></span></button>
                                </form>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="ctr-area">
        <div class="container">
            <div class="ctr-content">
                <h2>ATLANTIC APP</h2>
                <p>Clear. Simple. Easy. Free to use on all major SA networks, this means that ATLANTIC will pay for all the data that the App uses.</p>
                <a href="contact.html" class="default-btn">APP STORE <i class="flaticon-right-chevron"></i><span></span></a>
                <a href="contact.html" class="default-btn">GOOGLE APP STORE <i class="flaticon-right-chevron"></i><span></span></a>
            </div>
            <div class="ctr-image">
                <img src="{{asset('public/frontend/img/ctr-img.jpg')}}" alt="image">
            </div>
            <div class="shape">
                <img src="{{asset('public/frontend/img/bg-dot3.png')}}" alt="image">
            </div>
        </div>
    </section>

    <!-- Main content End -->


@endsection
