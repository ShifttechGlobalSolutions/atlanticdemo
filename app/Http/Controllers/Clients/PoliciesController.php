<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;

use App\Models\BeneficiaryInfo;
use App\Models\HolderInfo;
use App\Models\SpouseInfo;
use App\Models\Policy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PoliciesController extends Controller
{
    //
    public function index()
    {
        $policy_no=  HolderInfo::where('email', Auth()->User()->email)->value('policy_no');
        $policy_details=  HolderInfo::where('email', Auth()->User()->email)->get();
      //  $clientId = 'atfs-000';
        $user= Auth::user()->email;
        //$policy = Policy::latest()->where('email',$clientId)->paginate(10);
        $beneficiary = BeneficiaryInfo::latest()->where('policy_no',$policy_no)->paginate(10);
        $spouse = SpouseInfo::latest()->where('policy_no',$policy_no)->paginate(10);
       //dd($policy_no);

        //dd($claims);
//        return view('clients/claims',compact('claims'))
//
        return view('clients/policy')
            ->with('policy_details',$policy_details)
            ->with('beneficiaries',$beneficiary)
            ->with('spouses',$spouse) ;
    }

}
