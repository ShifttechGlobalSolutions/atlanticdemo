<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\Claims;

use Illuminate\Http\Request;

class ClaimsController extends Controller
{
    //
    public function index()
    {
        $claims = Claims::latest()->paginate(10);

        //dd($claims);
        return view('clients/claims',compact('claims'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function getClaimForm(){
        return view('clients/claimForm');
    }

    public function captureClaims(Request $request){
       // dd($request);
//        request()->validate([
//
//            'policyHolderId' => 'required',
//            'policyHolderSurname' => 'required',
//            'policyHolderFullName' => 'required',
//            'policyHolderDateOfBirth' => 'required',
//            'policyHolderEmail'=> 'required',
//            'policyHolderContactNumber' => 'required',
//            'policyHolderIdNumber'=> 'required',
//            'policyHolderPostalAddress'=> 'required',
//            'policyHolderResidentialAddress'=> 'required',
//            //deceasedDetails
//            'deceasedSurname'=> 'required',
//            'deceasedFullName'=> 'required',
//            'deceasedDateOfBirth'=> 'required',
//            'deceasedContactNumber'=> 'required',
//            'deceasedCauseOfDeath'=> 'required',
//            'deceasedLastAddress'=> 'required',
//            'deceasedDeathCertificateSerialNo'=> 'required',
//            'deceasedBiSerialNo'=> 'required',
//            'deceasedResidentialAddress'=> 'required',
//            //claimantDetails
//            'claimantSurname'=> 'required',
//            'claimantFullName'=> 'required',
//            'claimantContactNumber'=> 'required',
//            'claimantEmail'=> 'required',
//            'claimantIdNumber'=> 'required',
//            'claimantRelationship'=> 'required',
//            'claimantPostalAddress'=> 'required',
//            'claimantResidentialAddress'=> 'required',
//            //benefit payment options
//            'funeralParlourName'=> 'required',
//            'funeralParlourPhone'=> 'required',
//            //nominated beneficiary details
//            'beneficiaryName'=> 'required',
//            'beneficiaryIdNumber'=> 'required',
//            'beneficiaryBankName'=> 'required',
//            'beneficiaryBranch'=> 'required',
//            'beneficiaryAccountNumber'=> 'required',
//            'beneficiaryBranchCode'=> 'required',
//            'beneficiaryAccountType'=> 'required',
//
//        ]);
       // dd($request);

        Claims::create($request->all());

        return redirect()->route('clients/claims')
            ->with('success','Claim successfully submitted. We will get back to you soon. Thank you');

    }
}
