<?php

use App\Http\Controllers\VideoTutsController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PolicyController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ClaimController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;

use App\Http\Controllers\Clients\PoliciesController;
use App\Http\Controllers\Clients\BeneficieryController;
use App\Http\Controllers\Clients\ClaimsController;
use App\Http\Controllers\Clients\PaymentController;
use App\Http\Controllers\Clients\ProfileController;

use App\Http\Controllers\Agent\AgentCommisionsController;
use App\Http\Controllers\Agent\AgentDashboardController;
use App\Http\Controllers\Agent\AgentPoliciesController;

use App\Http\Controllers\Admin\AdminPoliciesController;
use App\Http\Controllers\Admin\AdminAgentsController;
use App\Http\Controllers\Admin\AdminClaimsController;
use App\Http\Controllers\Admin\AdminDashboardController;
use App\Http\Controllers\Admin\AdminNotificationsController;
use App\Http\Controllers\Admin\AdminReportsController;
use App\Http\Controllers\Admin\AdminProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//website_routes
Route::get('portal',[PortalController::class, 'index'])->name('portal');
Route::get('claims',[ClaimController::class, 'index'])->name('claims');
Route::get('myPolicy',[PolicyController::class, 'index'])->name('myPolicy');
Route::get('contact',[ContactController::class, 'index'])->name('contact');
Route::get('applications',[ApplicationController::class, 'index'])->name('applications');
Route::get('stepper_application',[ApplicationController::class, 'application'])->name('stepper_application');
Route::get('stepper-application',[ApplicationController::class, 'adminMail'])->name('stepper-application');
Route::post('adminMail',[ApplicationController::class, 'adminMail'])->name('admin-mail');

Auth::routes(['verify' => true]);
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('products', ProductController::class);
});


//client_routes
Route::get('clients/policy',[PoliciesController::class, 'index'])->name('clients/policy');
Route::get('clients/beneficiaries',[BeneficieryController::class, 'index'])->name('clients/beneficiaries');
Route::get('clients/payments',[PaymentController::class, 'index'])->name('clients/payments');
Route::get('clients/profile',[ProfileController::class, 'index'])->name('clients/profile');
Route::get('clients/claims',[ClaimsController::class, 'index'])->name('clients/claims');
Route::get('clients/claimForm',[ClaimsController::class, 'getClaimForm'])->name('clients/claimForm');
Route::post('clients/claimForm',[ClaimsController::class, 'captureClaims'])->name('claims.store');

//agent_routes
Route::get('agents/dashboard',[AgentDashboardController::class, 'index'])->name('agents/dashboard');
Route::get('agents/policy',[AgentPoliciesController::class, 'index'])->name('agents/policy');
Route::get('agents/commissions',[AgentCommisionsController::class, 'index'])->name('agents/commissions');
Route::get('agents/tutorials',[VideoTutsController::class, 'index'])->name('agents/tutorials');
Route::get('register',[RegisterController::class, 'index'])->name('register');
Route::get('login',[LoginController::class, 'index'])->name('login');

//admin_routes
Route::get('admin/dashboard',[AdminDashboardController::class, 'index'])->name('admin/dashboard');
Route::get('admin/policies',[AdminPoliciesController::class, 'index'])->name('admin/policies');
Route::get('admin/agents',[AdminAgentsController::class, 'index'])->name('admin/agents');
Route::get('admin/claims',[AdminClaimsController::class, 'index'])->name('admin/claims');
Route::get('admin/reports',[AdminReportsController::class, 'index'])->name('admin/reports');
Route::get('admin/notifications',[AdminNotificationsController::class, 'index'])->name('admin/notifications');
Route::get('admin/profile',[AdminProfileController::class, 'index'])->name('admin/profile');
Route::post('policy-filter',[AdminPoliciesController::class, 'filterByDate'])->name('policy-filter');
Route::get('clear-search',[AdminPoliciesController::class, 'clrSearch'])->name('clear-search');
Route::get('admin/delete/{id}',[AdminPoliciesController::class, 'destroy'])->name('admin/delete');
Route::get('admin/show/{id}',[AdminPoliciesController::class, 'show'])->name('admin/show');
Route::get('admin/edit/{id}',[AdminPoliciesController::class, 'show_stepper'])->name('admin/edit');
Route::post('admin/add-video',[VideoTutsController::class, 'store'])->name('admin/update-video');
Route::post('admin/update-video',[VideoTutsController::class, 'update'])->name('admin/add-video');
Route::get('admin/tutorials',[VideoTutsController::class, 'videoTuts'])->name('admin/tutorials');
Route::get('admin/delete-video/{id}',[VideoTutsController::class, 'deleteVideo'])->name('admin/delete-video');
Route::get('admin/show-video/{id}',[VideoTutsController::class, 'showVideo'])->name('admin/show-video');

