<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DependentInfo extends Model
{
    use HasFactory;

    protected $fillable = [
        "policy_no",
        'dep_fname',
        'dep_lname',
        'dep_dob',
        'dep_copiesofid',
    ];
}
