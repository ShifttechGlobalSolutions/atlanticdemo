<?php

namespace App\Http\Controllers;

use App\Models\BeneficiaryInfo;
use App\Models\Claims;
use Illuminate\Http\Request;
use App\Models\HolderInfo;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('auth');
       $this->middleware('verified');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        $policy=DB::table('holder_infos')->selectRaw('policy_no')->where('email', Auth()->User()->email)->value('policy_no');
        $policy_no=  HolderInfo::where('email', Auth()->User()->email)->value('policy_no');
        $policy_details=  HolderInfo::where('email', Auth()->User()->email)->get();
        //dd($policy_info);
       // $agents=  User::where('role',Auth::user()->hasRole('Agent'))->get();

       $allPolicies_count = HolderInfo::count();
        //$recentPolicies = HolderInfo::latest()->first()->take(10)->get();
        $recentPolicies= DB::table('holder_infos')->selectRaw('*')->paginate();
        //dd($recentPolicies);
          $allClaims = Claims::count();

        $recentClaims= DB::table('claims')->selectRaw('*')->paginate();
//        $allAgents = DB::table('agents')->count();
//
//        //agents
//        DB::table('items')->latest()->first();
//        $recentPolicies = DB::table('policies')->where('agent', Auth()->User()->email)->latest()->first()->take(10);
//        $agentPolicies = DB::table('policies')->where('agent', Auth()->User()->email)->count();
//        $agenTutorials = DB::table('tutorials')->count();
//        //get commission for agent -get total policies per month -then multiply by commision rate
//        $agentCommission = DB::table('policies')->count();

        //users

        $allBeneficiaries =BeneficiaryInfo::where('policy_no', $policy_no)->count();

       // $myPremiums = DB::table('payments')->where('policy_no', $policy_no)->sum('premiumPaid');
        $Beneficiaries =BeneficiaryInfo::where('policy_no', $policy_no)->get();
       // dd($allBeneficiaries);
//
        return view('home')
            ->with('count_beneficiaries',$allBeneficiaries)
            ->with('beneficiaries',$Beneficiaries)
            ->with('claims',$allClaims)
            ->with('recent_claims',$recentClaims)
            ->with('policy_details',$policy_details)
            ->with('all_policies_count',$allPolicies_count)
            ->with('recent_policies',$recentPolicies);

    }
}
