<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->id();
            $table->string('policyHolderId');
            $table->string('policyHolderSurname');
            $table->string('policyHolderFullName');
            $table->string('policyHolderDateOfBirth');
            $table->string('policyHolderEmail');
            $table->string('policyHolderContactNumber');
            $table->string('policyHolderIdNumber');
            $table->string('policyHolderPostalAddress');
            $table->string('policyHolderResidentialAddress');
            $table->string('deceasedSurname');
            $table->string('deceasedFullName');
            $table->string('deceasedDateOfBirth');
            $table->string('deceasedContactNumber');
            $table->string('deceasedCauseOfDeath');
            $table->string('deceasedLastAddress');

            $table->string('deceasedDeathCertificateSerialNo');
            $table->string('deceasedBiSerialNo');
            $table->string('deceasedResidentialAddress');
            $table->string('claimantSurname');
            $table->string('claimantFullName');
            $table->string('claimantContactNumber');
            $table->string('claimantEmail');
            $table->string('claimantIdNumber');
            $table->string('claimantRelationship');
            $table->string('claimantPostalAddress');
            $table->string('claimantResidentialAddress');
            $table->string('funeralParlourName');
            $table->string('funeralParlourPhone');
            $table->string('beneficiaryName');
            $table->string('beneficiaryIdNumber');
            $table->string('beneficiaryBankName');
            $table->string('beneficiaryBranch');
            $table->string('beneficiaryAccountNumber');
            $table->string('beneficiaryBranchCode');
            $table->string('beneficiaryAccountType');
            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
