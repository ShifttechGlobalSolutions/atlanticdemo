<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AgentCommisionsController extends Controller
{
    //
    public function index()
    {
        return view('agents/commissions');
    }
}
