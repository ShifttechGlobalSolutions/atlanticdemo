<header class="header-area">

    <div class="top-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12">
                    <ul class="top-header-nav">
                        <li><a href="#">COVID-19 Updates</a></li>

                        <li><a href="#">Our App</a></li>

                    </ul>
                </div>
                <div class="col-lg-7 col-md-12">
                    <div class="top-header-right-side">
                        <ul>
                            <li>
                                <div class="icon">
                                    <i class="flaticon-email"></i>
                                </div>
                                <span>Drop us an email:</span>
                                <a href="#"><span>info@atlanticfunerals.co.za</span></a>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="flaticon-call"></i>
                                </div>
                                <span>Call us:</span>
                                <a href="#">+27 84447 8850</a>
                            </li>
                            <li>
                                <a href="{{url('/login')}}" class="default-btn">LOGIN<span></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="navbar-area">
        <div class="pearo-responsive-nav">
            <div class="container">
                <div class="pearo-responsive-menu">
                    <div class="logo">
                        <a href="{{url('/')}}">
                            <img src="{{asset('public/frontend/img/logo.png')}}" alt="logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="pearo-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="{{url('/')}}">
                        <img src="{{asset('public/frontend/img/log.png')}}" alt="logo">
                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            {{--                            <li class="nav-item"><a href="{{url('/')}}" class="nav-link active">Home <i class="flaticon-down-arrow"></i></a>--}}
                            {{--                                <ul class="dropdown-menu">--}}
                            {{--                                    <li class="nav-item"><a href="index.html" class="nav-link active">Home One</a></li>--}}
                            {{--                                    <li class="nav-item"><a href="index-2.html" class="nav-link">Home Two</a></li>--}}
                            {{--                                    <li class="nav-item"><a href="index-3.html" class="nav-link">Home Three</a></li>--}}
                            {{--                                    <li class="nav-item"><a href="index-4.html" class="nav-link">Home Four</a></li>--}}
                            {{--                                </ul>--}}
                            {{--                            </li>--}}


                            <li class="nav-item"><a href="#" class="nav-link">Products <i class="flaticon-down-arrow"></i></a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a href="{{url('/')}}" class="nav-link">FUNERAL POLICIES</a></li>
                                    <li class="nav-item"><a href="{{url('/')}}" class="nav-link">FUNERAL ARRANGEMENTS</a></li>
                                    <li class="nav-item"><a href="{{url('/')}}" class="nav-link">CREMATIONS</a></li>
                                    <li class="nav-item"><a href="{{url('/')}}" class="nav-link">CASH FUNERALS</a></li>
                                    <li class="nav-item"><a href="{{url('/')}}" class="nav-link">TOMBSTONES</a></li>

                                </ul>
                            </li>
                            <li class="nav-item"><a href="{{url('/claims')}}" class="nav-link">Claims</a></li>

                            <li class="nav-item"><a href="{{url('/myPolicy')}}" class="nav-link">My Policy</a></li>
                            <li class="nav-item"><a href="{{url('/contact')}}" class="nav-link">Contact Us</a></li>

                        </ul>
                        <div class="others-option">
                            {{--                            <div class="option-item">--}}
                            {{--                                <i class="search-btn flaticon-search"></i>--}}
                            {{--                                <i class="close-btn flaticon-cross-out"></i>--}}
                            {{--                                <div class="search-overlay search-popup">--}}
                            {{--                                    <div class='search-box'>--}}
                            {{--                                        <form class="search-form">--}}
                            {{--                                            <input class="search-input" name="search" placeholder="Search" type="text">--}}
                            {{--                                            <button class="search-button" type="submit"><i class="flaticon-search"></i></button>--}}
                            {{--                                        </form>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
{{--                            <div class="burger-menu">--}}
{{--                                <i class="flaticon-menu"></i>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>

</header>
<div class="sidebar-modal">
    <div class="sidebar-modal-inner">
        <div class="sidebar-about-area">
            <div class="title">
                <h2>About Us</h2>
                <p>We believe brand interaction is key in communication. Real innovations and a positive customer experience are the heart of successful communication. No fake products and services. The customer is king, their lives and needs are the inspiration.</p>
            </div>
        </div>
        <div class="sidebar-instagram-feed">
            <h2>Instagram</h2>
            <ul>
                <li><a href="single-blog.html"><img src="{{asset('public/frontend/img/blog-image/1.jpg')}}" alt="image"></a></li>
                <li><a href="single-blog.html"><img src="{{asset('public/frontend/img/blog-image/2.jpg')}}" alt="image"></a></li>
                <li><a href="single-blog.html"><img src="{{asset('public/frontend/img/blog-image/3.jpg')}}" alt="image"></a></li>
                <li><a href="single-blog.html"><img src="{{asset('public/frontend/img/blog-image/4.jpg')}}" alt="image"></a></li>

            </ul>
        </div>
        <div class="sidebar-contact-area">
            <div class="sidebar-contact-info">
                <div class="contact-info-content">
                    <h2>
                        <a href="tel:+0881306298615">+088 130 629 8615</a>
                        <span>OR</span>
                        <a href="https://templates.envytheme.com/cdn-cgi/l/email-protection#01716460736e41666c60686d2f626e6c"><span class="__cf_email__" data-cfemail="22524743504d62454f434b4e0c414d4f">[email&#160;protected]</span></a>
                    </h2>
                    <ul class="social">
                        <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fab fa-youtube"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <span class="close-btn sidebar-modal-close-btn"><i class="flaticon-cross-out"></i></span>
    </div>
</div>
