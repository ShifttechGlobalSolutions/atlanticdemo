@extends("layouts.master")
@section("content")




    <div class="page-title-area page-title-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>My Policy</h2>
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li>My Policy</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="about-area ptb-100 bg-f8f8f8">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-lg-6 col-md-12">
                    <div class="about-content">

                        <h2>Create an ATLANTIC ID<span>to conviniently manage your own policy</span></h2>
                        <p>An ATLANTIC ID gives you access to the client portal and app. It's one simple login for everything.</p>
                        <div class="btn-box">
                            <a href="{{url('/stepper_application')}}" class="default-btn">CREATE MY ATLANTIC ID <span></span></a>
                            <a href="{{url('/login')}}" class="optional-btn">LOG IN <span></span></a>
                        </div>
                    </div>
                </div>

                {{--                                <div class="col-lg-6 col-md-12">--}}
                {{--                                    <div class="about-image">--}}
                {{--                                        <img src="{{asset('public/frontend/img/about-image/1.jpg')}}" alt="image">--}}
                {{--                                        <img src="{{asset('public/frontend/img/about-image/2.jpg')}}" alt="image">--}}
                {{--                                    </div>--}}
                {{--                                </div>--}}
            </div>

        </div>
    </section>
    <section class="services-area ptb-100 pb-70">
        <div class="container">
            <div class="section-title">
                <h2>Manage Your Policy</h2>
                {{--                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
            </div>
            <div class="row">

                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-services-box">
                        <div class="icon">
                            <i class="flaticon-insurance"></i>
                            <div class="icon-bg">
                                <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                                <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                            </div>
                        </div>
                        <h3><a href="{{url('/login')}}">Submit a claim</a></h3>
                        <p>Start a claim online through the Atlantic client portal or app.</p>
                        <a href="{{url('/claims')}}" class="read-more-btn">Find Out More</a>
                        <div class="box-shape">
                            <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-services-box">
                        <div class="icon">
                            <i class="flaticon-health-insurance"></i>
                            <div class="icon-bg">
                                <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                                <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                            </div>
                        </div>
                        <h3><a href="{{url('/login')}}">Amend your policy</a></h3>
                        <p>Making a change to your policy is simple, you wont even need to contact the call centre.</p>
                        <a href="{{url('/login')}}" class="read-more-btn">Update Policy</a>
                        <div class="box-shape">
                            <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-services-box">
                        <div class="icon">
                            <i class="flaticon-home-insurance"></i>
                            <div class="icon-bg">
                                <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                                <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                            </div>
                        </div>
                        <h3><a href="{{url('/login')}}">Your Documents</a></h3>
                        <p>Getting to know your insurance cover is as easy as requesting a document.</p>
                        <a href="{{url('/login')}}" class="read-more-btn">Find A Document</a>
                        <div class="box-shape">
                            <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                        </div>
                    </div>
                </div>

            </div>

            <div class="insurance-details-desc">
                <h3>Easy-to-use self-service features</h3>

                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <ul class="features-list">
                            <li><i class="fas fa-check"></i>Change Username and password</li>
                            <li><i class="fas fa-check"></i> Access your policy</li>
                            <li><i class="fas fa-check"></i> Amend your policy</li>

                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <ul class="features-list">
                            <li><i class="fas fa-check"></i> Add Beneficiaries</li>
                            <li><i class="fas fa-check"></i> Download Policy documentation</li>
                            <li><i class="fas fa-check"></i> Update Bank details</li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
