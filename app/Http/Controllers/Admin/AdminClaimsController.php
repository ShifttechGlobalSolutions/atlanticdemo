<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminClaimsController extends Controller
{
    public function index()
    {
        $allClaims = DB::table('claims')->get();
        return view('admin/claims')->with('allclaims', $allClaims);
    }

//    public function getAllClaims(){
//      $allClaims = DB::table('claims')->get();
//    }

    public function updateClaims(Request $request){

    }
}
