@extends("layouts.master")
@section("content")

    <div class="page-title-area page-title-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Applications</h2>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Applications</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <section class="services-area ptb-100 pb-70">
        <div class="container">
            <div class="section-title">
{{--                <h2>Buy Online</h2>--}}
                <h3>Let’s get started. This process takes approximately 10 minutes.</h3>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <div class="profile-menu">
                        <ul class="nav nav-tabs nav-tabs-solid">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#policy">Policy Holder Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#package">Package Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#spouse">Spouse Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#dependants">Dependants Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#beneficiary">Beneficiary Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#account">Account Details</a>
                            </li>

                        </ul>
                    </div>
                    <form  method="POST" action="{{ route('admin-mail')}}"  enctype="multipart/form-data" >
                        @CSRF
                        <div class="tab-content profile-tab-cont">
{{--                            <div class="tab-content">--}}
                            <div id="policy" class="tab-pane fade show active" >
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Policy Holder Details</h5>
                                        <div class="row">
                                            <div class="col-md-6 ">

                                                <div class="form-group">
                                                    <label>FirstName</label>
                                                    <input class="form-control " type="text"  name="fname"  required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Date of Birth</label>
                                                    <input name="dob1" type="date" class="form-control" required   >
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>LastName</label>
                                                    <input class="form-control " type="text"  name="lname"  required >
                                                </div>
                                                <div class="form-group">
                                                    <label>ID Number</label>
                                                    <input name="memID" class="form-control " type="text"   required>
                                                </div>
                                            </div>
                                            <div class="col-md-12 ">
                                                <div class="form-group">
                                                    <label>Copy of ID</label>
                                                    <input name="idCopy" type="file" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <input name="strName" class="form-control" type="text" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Postal Code</label>
                                                    <input name="pcode" class="form-control " type="text"  required>

                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>Suburb</label>
                                                    <input name="surburb" class="form-control " type="text"    required  >
                                                </div>
                                                <div class="form-group">
                                                    <label>Tel Number (Home)</label>
                                                    <input name="homeContact" class="form-control "  required  >
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>Tel Number (Work)</label>
                                                    <input name="workContact" class="form-control " type="text"  required >
                                                </div>
                                                <div class="form-group">
                                                    <label>Email Address</label>
                                                    <input name="emailContact" required   class="form-control">
                                                </div>

                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>Cellphone</label>
                                                    <input name="cellContact" class="form-control" required   >
                                                </div>
                                                <div class="form-group">
                                                    <label for="email"><strong>Gender:</strong></label><br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="genderRadio" id="gridRadios1" value="Male" checked>&nbsp;<span>Male</span><br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="genderRadio" id="gridRadios2" value="Female">&nbsp;<span>Female</span>

                                            </div>

                                        </div>

                                        <div style="text-align: left">
                                            <button data-toggle="tab" href="#package" class="btn btn-primary"  >Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div id="package" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title" id="modal_body"></h5>
                                        <div class="row">


                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>Age Group</label>
                                                    <input id="modal_body1" class="form-control " type="hidden"  name="package">
                                                        <select name="ageGroup" class="form-control" required>
                                                            <option>Select Age Group</option>
                                                            <option value="14 - 64 yrs">14 - 64 yrs</option>
                                                            <option value="65 - 74 yrs">65 - 74 yrs</option>
                                                            <option value="75 - 85 yrs">75 - 85  yrs</option>
                                                        </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Cover Amount</label>
                                                    <select name="coverAmount" class="form-control" required>
                                                        <option>Select Cover Amount</option>
                                                        <option value="5000">R 5 000</option>
                                                        <option value="7000">R 7 000</option>
                                                        <option value="10000">R 10 000</option>
                                                        <option value="13000">R 13 000</option>
                                                    </select>
                                                </div>

                                            </div>

                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label for="email"><strong>Status:</strong></label><br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="marital" id="marital1" value="Single" checked>&nbsp;<span>Single</span><br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="marital" id="marital2" value="Married">&nbsp;<span>Married</span>
                                                </div>
                                            </div>

                                        </div>
                                        <BR>
                                        <div style="text-align: left">
                                            <button data-toggle="tab" href="#spouse" class="btn btn-primary"  >Next</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="spouse" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Spouse Details</h5>
                                        <div class="row">
                                            <div class="col-md-6 ">

                                                <div class="form-group">
                                                    <label>FirstName</label>
                                                    <input name="sfname" type="text" class="form-control" required >
                                                </div>
                                                <div class="form-group">
                                                    <label>ID Number</label>
                                                    <input name="sId" type="text" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>LastName</label>
                                                        <input name="slname" class="form-control " type="text"   required>
                                                    </div>
                                                    <label>Date of Birth</label>
                                                    <input name="sdob" class="form-control " type="date"   required  >
                                                </div>
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Contact Number</label>--}}
{{--                                                    <input type="tel" name="claimantContactNumber" class="form-control">--}}
{{--                                                </div>--}}
                                            </div>
{{--                                            <div class="col-md-6 ">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Email Address</label>--}}
{{--                                                    <input type="email" name="claimantEmail" class="form-control">--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>ID Number</label>--}}
{{--                                                    <input type="text" name="claimantIdNumber" class="form-control">--}}
{{--                                                </div>--}}


{{--                                            </div>--}}
{{--                                            <div class="col-md-6 ">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Relationship</label>--}}
{{--                                                    <input type="text" name="claimantRelationship" class="form-control">--}}
{{--                                                </div>--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Postal Address</label>--}}
{{--                                                    <input type="text" name="claimantPostalAddress" class="form-control">--}}
{{--                                                </div>--}}

{{--                                            </div>--}}
{{--                                            <div class="col-md-6 ">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Residential Address</label>--}}
{{--                                                    <input type="text" name="claimantResidentialAddress" class="form-control">--}}
{{--                                                </div>--}}

{{--                                                <div class=" form-group ">--}}
{{--                                                    <label>Claimant Copy of Id</label>--}}
{{--                                                    <input name="claimantIdCopy" type="file" class="form-control"  >--}}
{{--                                                </div>--}}


{{--                                            </div>--}}

{{--                                            <div class="col-md-6">--}}
{{--                                                <div class=" form-group">--}}
{{--                                                    <label>Police Report Copy</label>--}}
{{--                                                    <input name="policeReportCopy" type="file" class="form-control" >--}}
{{--                                                </div>--}}
{{--                                            </div>--}}





                                        </div>

                                        <div style="text-align: left">
                                            <button data-toggle="tab" href="#dependants" class="btn btn-primary"  >Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="dependants" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Dependant(s) Options</h5>
                                        <div class="inc">
                                        <div class="row">
                                            <div class="form-group col-md-6 ">
                                                <label>FirstName</label>
                                                <input  name="dep_fname" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6 ">
                                                <label>LastName</label>
                                                <input name="dep_lname"  class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6 ">
                                                <label>Date Of Birth</label>
                                                <input  type="date" name="dep_dob" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6 ">
                                                <label>Birth Certificate Copy</label>
                                                <input  type="file" name="depIdCody" id="depIdCody"  class="form-control">
                                            </div>
                                        </div>
                                        <hr>
                                        </div>
                                    <div class="row" style="text-align: right">
                                        <div class="col">
                                            <button  class="btn btn-info" type="submit" id="append" name="append"><i class="fa fa-plus" aria-hidden="true"></i>  Dependent</button>
                                        </div>
                                    </div><br>
                                        <div>
                                            <div style="text-align: left">
                                                <button data-toggle="tab" href="#beneficiary" class="btn btn-primary"  >Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div id="beneficiary" class="tab-pane fade">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Beneficiary Details</h5>
                                        <div class="row">
                                            <div class="form-group col">
                                                <label>FirstName</label>
                                                <input name="benfname" type="text" class="form-control" required>
                                            </div>
                                            <div class="form-group col">
                                                <label>LastName</label>
                                                <input name="benlname" class="form-control " type="text"   required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col">
                                                <label>ID Number</label>
                                                <input id="benId" name="benId" type="text" class="form-control" required >
                                            </div>
                                            <div class="form-group col">
                                                <label>Data of Birth</label>
                                                <input id="bdob" name="bdob" class="form-control " type="date"   required  >
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col">
                                                <label>Contact Number</label>
                                                <input id="benconNumber" name="benconNumber" required class="form-control">
                                            </div>
{{--                                            <div class="form-group col">--}}
{{--                                                <label>Beneficiary Branch Code</label>--}}
{{--                                                <input  name="beneficiaryBranchCode" class="form-control " type="text"    >--}}
{{--                                            </div>--}}
                                        </div>
                                        <br>
                                        <div>
                                            <div style="text-align: left">
                                                <button data-toggle="tab" href="#account" class="btn btn-primary"  >Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="account" class="tab-pane fade">
                                 <div class="card">
                                     <div class="card-body">
                                          <h5 class="card-title">Account Details</h5>
                                          <div class="row">
                                              <div class="form-group col">
                                                  <label>Bank Name</label>
                                                  <input name="bankName" type="text" class="form-control" required>
                                              </div>
                                              <div class="form-group col">
                                                  <label>Branch Name</label>
                                                  <input name="branchName" class="form-control " type="text"   required>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="form-group col">
                                                   <label>Account Number</label>
                                                  <input name="accNumber" type="text" class="form-control" required >
                                              </div>
                                              <div class="form-group col">
                                                   <label>Branch code</label>
                                                  <input name="branchCode" class="form-control " type="text"   required>
                                              </div>
                                          </div>

                                          <br>

                                        <label for="email"><strong>Account type:</strong></label>

                                        <div class="row" >
                                            <div class="form-group col-md-4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType"   value="Current" checked>&nbsp;<span>Current (cheque)</span><br>
                                            </div>
                                            <div class="form-group col-md-4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType"   value="Savings">&nbsp;<span>Savings</span><br>
                                            </div>
                                            <div class="form-group col-md-4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType"   value="Transmission">&nbsp;<span>Transmission</span>
                                            </div>

                                        </div>
{{--                                        <div class="row" >--}}
{{--                                            <div class="custom-file col ">--}}
{{--                                                <label>Bank Statement Copy</label>--}}
{{--                                                <input name="bankStatementCopy" type="file" class="form-control" id="customFile">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                         <br><br>
                                        <div style="text-align: left">
                                            <button type="submit" class="btn btn-primary"  >Submit Claim</button>
                                        </div>


                                    </div>
                                 </div>

                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>

{{--            demo end--}}
{{--            <div class="row ">--}}

{{--                <div class="col-lg-12 col-md-12 col-sm-12">--}}
{{--                    <div class="single-services-box">--}}

{{--                        <form id="regForm" method="POST" action="{{ route('admin-mail')}}"  enctype="multipart/form-data" >--}}
{{--                        @CSRF--}}
{{--                            <!-- One "tab" for each step in the form: -->--}}
{{--                            <div class="tab"><h6>Policy Holder Details:</h6>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="firstName" class="form-control " type="text"  name="fname"  required   placeholder=" Firstname">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="lastName" class="form-control " type="text"  name="lname"  required   placeholder=" Lastname">--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="dob1" name="dob1" type="date" class="form-control" required   >--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="memID" name="memID" class="form-control " type="text"   required   placeholder="ID. Number">--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="row">--}}
{{--                                    <div class="custom-file">--}}
{{--                                        <input name="idCopy" type="file" class="form-control" id="customFile" placeholder="Copy of ID">--}}
{{--                                    </div>--}}
{{--                                </div><br>--}}

{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="strName" name="strName" class="form-control" type="text" required   placeholder="Street Name">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="surburb" name="surburb" class="form-control " type="text"    required   placeholder="Surburb">--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="pcode" name="pcode" class="form-control " type="text"  required   placeholder="Postal Code">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="home" name="homeContact" class="form-control " placeholder="Home: (000) 000 0000" required  >--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="work" name="workContact" class="form-control " type="text"  required   placeholder="Work: (000) 000 0000">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="cell" name="cellContact" class="form-control " placeholder="cell: (000) 000 0000" required   >--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="email" name="emailContact" placeholder="example@gmail.com" required   class="form-control">--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="row" style="text-align: left">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <label for="email"><strong>Gender:</strong></label><br>--}}
{{--                                        &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="genderRadio" id="gridRadios1" value="Male" checked>&nbsp;<span>Male</span><br>--}}
{{--                                        &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="genderRadio" id="gridRadios2" value="Female">&nbsp;<span>Female</span>--}}

{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}


{{--                            <div class="tab"><h6>Package</h6><br>--}}
{{--                                <div class="row" style="text-align: left">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="modal_body1" class="form-control " type="hidden"  name="package">--}}
{{--                                        <select name="ageGroup" class="form-control" required>--}}
{{--                                            <option>Select Age Group</option>--}}
{{--                                            <option value="14-64 yrs">14-64 yrs</option>--}}
{{--                                            <option value="65-74 yrs">65-74 yrs</option>--}}
{{--                                            <option value="75-85 yrs">75-85  yrs</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <label for="email"><strong>Status:</strong></label><br>--}}
{{--                                        &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="marital" id="marital1" value="Single" checked>&nbsp;<span>Single</span><br>--}}
{{--                                        &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="marital" id="marital2" value="Married">&nbsp;<span>Married</span>--}}

{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="row" style="text-align: left">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <select name="coverAmount" class="form-control" required>--}}
{{--                                            <option>Select Cover Amount</option>--}}
{{--                                            <option value="5000">R 5 000</option>--}}
{{--                                            <option value="7000">R 7 000</option>--}}
{{--                                            <option value="10000">R 10 000</option>--}}
{{--                                            <option value="13000">R 13 000</option>--}}
{{--                                        </select>--}}

{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                        <div class="tab"><h6>Spouse Details:</h6>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="sfname" name="sfname" type="text" class="form-control" required   placeholder="Firstname">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="slname" name="slname" class="form-control " type="text"   required   placeholder="Lastname">--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="sId" name="sId" type="text" class="form-control" required   placeholder="ID. Number">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="sdob" name="sdob" class="form-control " type="date"   required  >--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="tab"><h6>Dependants Details:</h6>--}}
{{--                                <div class="inc">--}}
{{--                                <div class="row" style="text-align: left">--}}
{{--                                    <div class="col">--}}
{{--                                        <label>Last Name</label><br>--}}
{{--                                        <input  name="dep_fname" class="form-control">--}}
{{--                                    </div>--}}
{{--                                    <div class="col">--}}
{{--                                        <label>First Name</label><br>--}}
{{--                                        <input name="dep_lname"  class="form-control">--}}
{{--                                    </div>--}}
{{--                                </div><br>--}}

{{--                                <div class="row" style="text-align: left">--}}
{{--                                    <div class="col">--}}
{{--                                        <label>Date Of Birth</label><br>--}}
{{--                                        <input  type="date" name="dep_dob" class="form-control">--}}
{{--                                    </div>--}}
{{--                                    <div class="col">--}}
{{--                                        <label>Birth Certificate Copy</label><br>--}}
{{--                                        <input  type="file" name="depIdCody" id="depIdCody"  class="form-control">--}}
{{--                                    </div>--}}
{{--                                </div><br>--}}

{{--                                <hr>--}}
{{--                                </div>--}}
{{--                                <div class="row" style="text-align: right">--}}
{{--                                    <div class="col">--}}
{{--                                        <button  class="btn btn-info" type="submit" id="append" name="append"><i class="fa fa-plus" aria-hidden="true"></i>  Dependent</button>--}}
{{--                                    </div>--}}
{{--                                </div><br>--}}
{{--                            </div>--}}

{{--                            <div class="tab"><h6>Beneficiary Details:</h6>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="benfname" name="benfname" type="text" class="form-control" required   placeholder="Firstname">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="benlname" name="benlname" class="form-control " type="text"   required   placeholder="Lastname">--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="benId" name="benId" type="text" class="form-control" required   placeholder="ID. Number">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="bdob" name="bdob" class="form-control " type="date"   required  >--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="benconNumber" name="benconNumber" required   placeholder="Call: (000) 000 0000" class="form-control">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="tab"><h6>Account Details:</h6>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="bankName" name="bankName" type="text" class="form-control" required   placeholder="Name of bank">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="branchName" name="branchName" class="form-control " type="text"   required   placeholder="Name of branch">--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="row">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="accNumber" name="accNumber" type="text" class="form-control" required   placeholder="Account number">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <input id="branchCode" name="branchCode" class="form-control " type="text"   required   placeholder="Branch code--}}
{{--">--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="row" style="text-align: left">--}}
{{--                                    <div class="form-group col">--}}
{{--                                        <label for="email"><strong>Account type:</strong></label><br>--}}
{{--                                        &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType" id="accType1" value="Current" checked>&nbsp;<span>Current (cheque)</span><br>--}}
{{--                                        &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType" id="accType2" value="Savings">&nbsp;<span>Savings</span><br>--}}
{{--                                        &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType" id="accType3" value="Transmission">&nbsp;<span>Transmission</span>--}}
{{--                                    </div>--}}

{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div style="overflow:auto;">--}}
{{--                                <div style="float:right;">--}}
{{--                                    <button type="button" class="btn btn-secondary" id="prevBtn" onclick="nextPrev(-1)">Previous</button>--}}
{{--                                    <button type="button" class="btn btn-primary" id="nextBtn" onclick="nextPrev(1)">Next</button>--}}
{{--                                    <button type="submit" class="btn btn-primary"  >Submit</button>--}}

{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- Circles which indicates the steps of the form: -->--}}
{{--                            <div style="text-align:center;margin-top:40px;">--}}
{{--                                <span class="step"></span>--}}
{{--                                <span class="step"></span>--}}
{{--                                <span class="step"></span>--}}
{{--                                <span class="step"></span>--}}
{{--                                <span class="step"></span>--}}
{{--                                <span class="step"></span>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                        <div class="box-shape">--}}
{{--                            <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">--}}
{{--                            <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}



{{--        </div>--}}
    </section>

    <!-- Modal HTML -->
    <div id="generalMembership" class="modal fade">
        <div class="modal-dialog modal-login">
            <div class="modal-content">
                <div class="modal-header">
                    {{--                    <div class="avatar">--}}
                    {{--                        <img src="{{asset('public/frontend/img/atlantic_logo.png')}}" alt="" class="img-fluid">--}}
                    {{--                    </div>--}}
                    {{--                    <h4 class="modal-title">General Membership Policy</h4>--}}
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="single-pricing-box">
                                <div class="pricing-header bg1">
                                    <h3>General Membership Policy</h3>
                                </div>
                                <div class="price">
                                    <sub></sub>R.179<sub>/mo</sub>
                                </div>

                                <ul class="pricing-features-list">
                                    <li><i class="fas fa-check"></i> Home Insurance</li>
                                    <li><i class="fas fa-check"></i> Business Insurance</li>
                                    <li><i class="fas fa-check"></i> Health Insurance</li>
                                    <li><i class="fas fa-check"></i> Travel Insurance</li>
                                    <li><i class="flaticon-cross-out"></i> Car Insurance</li>
                                    <li><i class="flaticon-cross-out"></i> Life Insurance</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="book-now-btn">
                        <a href="{{url('/stepper_application')}}" class="default-btn">BUY ONLINE <span></span></a>
                        <a href="#" class="default-btn">Call Me Back <span></span></a>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="executiveMembership" class="modal fade">
        <div class="modal-dialog modal-login">
            <div class="modal-content">
                <div class="modal-header">
                    {{--                    <div class="avatar">--}}
                    {{--                        <img src="{{asset('public/frontend/img/atlantic_logo.png')}}" alt="" class="img-fluid">--}}
                    {{--                    </div>--}}
                    {{--                    <h4 class="modal-title">Member Login</h4>--}}
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="single-pricing-box">
                                <div class="pricing-header bg1">
                                    <h3>Executive Membership Policy</h3>
                                </div>
                                <div class="price">
                                    <sub></sub>R.179<sub>/mo</sub>
                                </div>

                                <ul class="pricing-features-list">
                                    <li><i class="fas fa-check"></i> Home Insurance</li>
                                    <li><i class="fas fa-check"></i> Business Insurance</li>
                                    <li><i class="fas fa-check"></i> Health Insurance</li>
                                    <li><i class="fas fa-check"></i> Travel Insurance</li>
                                    <li><i class="flaticon-cross-out"></i> Car Insurance</li>
                                    <li><i class="flaticon-cross-out"></i> Life Insurance</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="book-now-btn">
                        <a href="{{url('/stepper_application')}}" class="default-btn">BUY ONLINE <span></span></a>
                        <a href="#" class="default-btn">Call Me Back <span></span></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="callMeBack" class="modal fade">
        <div class="modal-dialog modal-login">
            <div class="modal-content">
                <div class="modal-header">
                    {{--                    <div class="avatar">--}}
                    {{--                        <img src="{{asset('public/frontend/img/atlantic_logo.png')}}" alt="" class="img-fluid">--}}
                    {{--                    </div>--}}
                    {{--                    <h4 class="modal-title">Member Login</h4>--}}
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="single-pricing-box">
                                <div class="pricing-header bg1">
                                    <h3>CALL ME BACK</h3>
                                </div>


                                <div class="col-lg-12 col-md-12">
                                    <div class="subscribe-form">
                                        <form class="newsletter-form" data-toggle="validator">
                                            <input type="email" class="input-newsletter" placeholder="Enter your phone number" name="EMAIL" required autocomplete="off">

                                            <div id="validator-newsletter" class="form-result"></div>
                                            <div class="book-now-btn">

                                                <a href="#" class="default-btn">Call Me Back <span></span></a>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
        if (typeof(Storage) !== "undefined") {
            // Retrieve
            document.getElementById("modal_body").innerHTML = localStorage.getItem("policy");
            document.getElementById("modal_body1").value = localStorage.getItem("policy");
        } else {
            document.getElementById("result").innerHTML = "Browser does not support Web Storage.";
        }
    </script>
@endsection
