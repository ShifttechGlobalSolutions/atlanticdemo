@component('mail::message')

    Thank you for submitting your application, we will get back at you soonest!!!


    @component('mail::button', ['url' => 'http://127.0.0.1/atlanticfuneral/portal/register'])
        Button Text
    @endcomponent

    Thanks,
    Atlantic Funeral Services
@endcomponent
