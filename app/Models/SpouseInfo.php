<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpouseInfo extends Model
{
    use HasFactory;

    protected $fillable = [
        'policy_no',
        'spouse_firstname',
        'spouse_lastname',
        'spouse_idNumber',
        'spouse_dob'
    ];
}
