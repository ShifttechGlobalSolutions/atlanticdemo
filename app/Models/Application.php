<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    protected $fillable = [
        'policyHolderName',
        'policyHolderSurname',
        'policyHolderIdNumber',
        'policyHolderAddress',
        'policyHolderContact',
        'policyHolderEmail',
        'policyHolderGender',
        'policyHolderSpouseName',
        'policyHolderSpouseSurname',
        'policyHolderSpouseIdNumber',
        'policyHolderSpouseDateOfBirth',
        'policyHolderSpouseGender',
        'policyHolderDependantName',
        'policyHolderDependantSurname',
        'policyHolderDependantIdNumber',
        'policyHolderDependantDateOfBirth',
        'policyHolderBeneficiaryName',
        'policyHolderBeneficiarySurname',
        'policyHolderBeneficiaryIdNumber',
        'policyHolderBeneficiaryContact',
        'username',
        'email',
        'password',
    ];
}
