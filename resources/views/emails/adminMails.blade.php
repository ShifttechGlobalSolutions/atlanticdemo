@component('mail::message')
    # Introduction

    <h3>Executive Member</h3><br>
    @component('mail::button', ['url' => ''])
        <table>
            <tr>
                <th>First Name</th>
                <td>{{ $data['memberFirstName'] }}</td>
            </tr>
            <tr>
                <th>Last Name</th>
                <td>{{ $data['memberLastName'] }}</td>
            </tr>
            <tr>
                <th>D.O.B</th>
                <td>{{ $data['memberDOB'] }}</td>
            </tr>
            <tr>
                <th>ID. Number</th>
                <td>{{ $data['memberIDNum'] }}</td>
            </tr>

            <tr>
                <th>Street Name</th>
                <td>{{ $data['memberStrName'] }}</td>
            </tr>
            <tr>
                <th>Surburb</th>
                <td>{{ $data['memberSurburb'] }}</td>
            </tr>

            <tr>
                <th>Postal Code</th>
                <td>{{ $data['memberPcode'] }}</td>
            </tr>
            <tr>
                <th>Tel: Home</th>
                <td>{{ $data['memberHomeContact'] }}</td>
            </tr>
            <tr>
                <th>Tel: Work</th>
                <td>{{ $data['memberWorkContact'] }}</td>
            </tr>
            <tr>
                <th>Cell</th>
                <td>{{ $data['memberCellContact'] }}</td>
            </tr>

            <tr>
                <th>Email</th>
                <td>{{ $data['memberEmailContact'] }}</td>
            </tr>
            <tr>
                <th>Gender</th>
                <td>{{ $data['memberGenderRadio'] }}</td>
            </tr>

            <tr>
                <th>Spouse First Name</th>
                <td>{{ $data['spouseFname'] }}</td>
            </tr>
            <tr>
                <th>Spouse Last Name</th>
                <td>{{ $data['spouseLname'] }}</td>
            </tr>
            <tr>
                <th>Spouse ID. Number</th>
                <td>{{ $data['spouseId'] }}</td>
            </tr>
            <tr>
                <th>Spouse D.O.B</th>
                <td>{{ $data['spouseDOB'] }}</td>
            </tr>

            <tr>
                <th>Email</th>
                <td>{{ $data['memberEmailContact'] }}</td>
            </tr>
            <tr>
                <th>Gender</th>
                <td>{{ $data['memberGenderRadio'] }}</td>
            </tr>

            <tr>
                <th>Beneficiary First Name</th>
                <td>{{ $data['benFname'] }}</td>
            </tr>
            <tr>
                <th>Beneficiary Last Name</th>
                <td>{{ $data['benLname'] }}</td>
            </tr>
            <tr>
                <th>Beneficiary Contact Number</th>
                <td>{{ $data['benConNumber'] }}</td>
            </tr>
        </table>
        <br>
        <table>
            <tr>
                <th>#</th>
                <th>Dependent First Name</th>
                <th>Dependent Last Name</th>
                <th>D.O.B</th>
            </tr>

        </table>
            @endcomponent

            Thanks,
            {{ config('app.name') }}

        @endcomponent
