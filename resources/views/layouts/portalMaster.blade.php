<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Atlantic Portal | Client Portal</title>
    <link rel="shortcut icon" href="{{asset('public/frontend/img/log.png')}}">
    <link rel="stylesheet" href="{{asset('public/portal/assets/plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/portal/assets/plugins/fontawesome/css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/portal/assets/plugins/fontawesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/portal/assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/portal/assets/css/admin.css')}}">
</head>
<body>
@include("patials.portalHeader")


@yield('content')


@include("patials.portalFooter")

    <script src="{{asset('public/portal/assets/js/jquery-3.5.0.min.js')}}"></script>

    <script src="{{asset('public/portal/assets/js/popper.min.js')}}"></script>
    <script src="{{asset('public/portal/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

    <script src="{{asset('public/portal/assets/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

    <script src="{{asset('public/portal/assets/js/admin.js')}}"></script>



</body>


</html>
