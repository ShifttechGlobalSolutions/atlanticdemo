<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Claims extends Model
{
    use HasFactory;

    protected $fillable = [
        'policyHolderId',
        'policyHolderSurname',
        'policyHolderFullName',
        'policyHolderDateOfBirth',
        'policyHolderEmail',
        'policyHolderContactNumber',
        'policyHolderIdNumber',
        'policyHolderPostalAddress',
        'policyHolderResidentialAddress',
        //deceasedDetails
        'deceasedSurname',
        'deceasedFullName',
        'deceasedContactNumber',
        'deceasedDateOfBirth',
        'deceasedLastAddress',
        'deceasedCauseOfDeath',
        'deceasedResidentialAddress',
        'deceasedBiSerialNo',
        'deceasedDeathCertificateSerialNo',
        //claimantDetails
        'claimantSurname',
        'claimantFullName',
        'claimantContactNumber',
        'claimantEmail',
        'claimantIdNumber',
        'claimantRelationship',
        'claimantPostalAddress',
        'claimantResidentialAddress',
        //benefit payment options
        'funeralParlourName',
        'funeralParlourPhone',
        //nominated beneficiary details
        'beneficiaryName',
        'beneficiaryIdNumber',
        'beneficiaryBankName',
        'beneficiaryBranch',
        'beneficiaryAccountNumber',
        'beneficiaryBranchCode',
        'beneficiaryAccountType',

    ];
}
