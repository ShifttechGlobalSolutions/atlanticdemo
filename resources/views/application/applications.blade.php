@extends("layouts.master")
@section("content")




    <div class="page-title-area page-title-bg1">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h2>Applications</h2>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Applications</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    <section class="about-area ptb-100 bg-f8f8f8">--}}
{{--        <div class="container">--}}
{{--            <div class="row align-items-center">--}}

{{--                <div class="col-lg-6 col-md-12">--}}
{{--                    <div class="about-content">--}}

{{--                        <h2>Create an ATLANTIC ID<span>to conviniently manage your own policy</span></h2>--}}
{{--                        <p>An ATLANTIC ID gives you access to the client portal and app. It's one simple login for everything.</p>--}}
{{--                        <div class="btn-box">--}}
{{--                            <a href="pricing.html" class="default-btn">CREATE MY ATLANTIC ID <span></span></a>--}}
{{--                            <a href="contact.html" class="optional-btn">LOG IN <span></span></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                --}}{{--                                <div class="col-lg-6 col-md-12">--}}
{{--                --}}{{--                                    <div class="about-image">--}}
{{--                --}}{{--                                        <img src="{{asset('public/frontend/img/about-image/1.jpg')}}" alt="image">--}}
{{--                --}}{{--                                        <img src="{{asset('public/frontend/img/about-image/2.jpg')}}" alt="image">--}}
{{--                --}}{{--                                    </div>--}}
{{--                --}}{{--                                </div>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </section>--}}





    <section class="services-area ptb-100 pb-70">
        <div class="container">
            <div class="section-title">
                <h2>Lets get started</h2>
                                <p>What do you need?</p>
            </div>
            <div class="row quote-boxes-area">

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="single-services-box">
                        <div class="icon">
                            <i class="flaticon-insurance"></i>
                            <div class="icon-bg">
                                <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                                <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                            </div>
                        </div>
                        <h3><a href="#callMeBack" data-toggle="modal">Help me choose a Plan</a></h3>
                        <p>Not sure which plan would suit your needs? Let us ask you a few questions that will help us assist you with making your decision.</p>
                        <div class="btn-box">
                            <a href="#callMeBack"  data-toggle="modal" class="default-btn">SELECT <span></span></a>

                        </div>
                        <div class="box-shape">
                            <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="single-services-box">
                        <div class="icon">
                            <i class="flaticon-health-insurance"></i>
                            <div class="icon-bg">
                                <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                                <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                            </div>
                        </div>
                        <h3><a href="{{url('/stepper_application')}}" >General Membership Policy</a></h3>

                        <div class="btn-box">

                            <a href="{{url('/stepper_application')}}"  class="default-btn">SELECT <span></span></a>
                        </div>
                        <div class="box-shape">
                            <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="single-services-box">
                        <div class="icon">
                            <i class="flaticon-home-insurance"></i>
                            <div class="icon-bg">
                                <img src="{{asset('public/frontend/img/icon-bg1.png')}}" alt="image">
                                <img src="{{asset('public/frontend/img/icon-bg2.png')}}" alt="image">
                            </div>
                        </div>
                        <h3><a href="{{url('/stepper_application')}}">Executive Membership Policy</a></h3>

                        <div class="btn-box">

                            <a href="{{url('/stepper_application')}}"  class="default-btn">SELECT <span></span></a>
                        </div>
                        <div class="box-shape">
                            <img src="{{asset('public/frontend/img/box-shape1.png')}}" alt="image">
                            <img src="{{asset('public/frontend/img/box-shape2.png')}}" alt="image">
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </section>



    <!-- Modal HTML -->
    <div id="generalMembership" class="modal fade">
        <div class="modal-dialog modal-login">
            <div class="modal-content">
                <div class="modal-header">
{{--                    <div class="avatar">--}}
{{--                        <img src="{{asset('public/frontend/img/atlantic_logo.png')}}" alt="" class="img-fluid">--}}
{{--                    </div>--}}
{{--                    <h4 class="modal-title">General Membership Policy</h4>--}}
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="single-pricing-box">
                            <div class="pricing-header bg1">
                                <h3>General Membership Policy</h3>
                            </div>
                            <div class="price">
                                <sub></sub>R.179<sub>/mo</sub>
                            </div>

                            <ul class="pricing-features-list">
                                <li><i class="fas fa-check"></i> Home Insurance</li>
                                <li><i class="fas fa-check"></i> Business Insurance</li>
                                <li><i class="fas fa-check"></i> Health Insurance</li>
                                <li><i class="fas fa-check"></i> Travel Insurance</li>
                                <li><i class="flaticon-cross-out"></i> Car Insurance</li>
                                <li><i class="flaticon-cross-out"></i> Life Insurance</li>
                            </ul>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="book-now-btn">
                        <a href="{{url('/stepper_application')}}" class="default-btn">BUY ONLINE <span></span></a>
                        <a href="#" class="default-btn">Call Me Back <span></span></a>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="executiveMembership" class="modal fade">
        <div class="modal-dialog modal-login">
            <div class="modal-content">
                <div class="modal-header">
{{--                    <div class="avatar">--}}
{{--                        <img src="{{asset('public/frontend/img/atlantic_logo.png')}}" alt="" class="img-fluid">--}}
{{--                    </div>--}}
{{--                    <h4 class="modal-title">Member Login</h4>--}}
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="single-pricing-box">
                                <div class="pricing-header bg1">
                                    <h3>Executive Membership Policy</h3>
                                </div>
                                <div class="price">
                                    <sub></sub>R.179<sub>/mo</sub>
                                </div>

                                <ul class="pricing-features-list">
                                    <li><i class="fas fa-check"></i> Home Insurance</li>
                                    <li><i class="fas fa-check"></i> Business Insurance</li>
                                    <li><i class="fas fa-check"></i> Health Insurance</li>
                                    <li><i class="fas fa-check"></i> Travel Insurance</li>
                                    <li><i class="flaticon-cross-out"></i> Car Insurance</li>
                                    <li><i class="flaticon-cross-out"></i> Life Insurance</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="book-now-btn">
                        <a href="{{url('/stepper_application')}}" class="default-btn">BUY ONLINE <span></span></a>
                        <a href="#" class="default-btn">Call Me Back <span></span></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="callMeBack" class="modal fade">
        <div class="modal-dialog modal-login">
            <div class="modal-content">
                <div class="modal-header">
{{--                    <div class="avatar">--}}
{{--                        <img src="{{asset('public/frontend/img/atlantic_logo.png')}}" alt="" class="img-fluid">--}}
{{--                    </div>--}}
{{--                    <h4 class="modal-title">Member Login</h4>--}}
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="single-pricing-box">
                                <div class="pricing-header bg1">
                                    <h3>CALL ME BACK</h3>
                                </div>


                                <div class="col-lg-12 col-md-12">
                                    <div class="subscribe-form">
                                        <form class="newsletter-form" data-toggle="validator">
                                            <input type="email" class="input-newsletter" placeholder="Enter your phone number" name="EMAIL" required autocomplete="off">

                                            <div id="validator-newsletter" class="form-result"></div>
                                            <div class="book-now-btn">

                                                <a href="#" class="default-btn">Call Me Back <span></span></a>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



@endsection
