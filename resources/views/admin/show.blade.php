@extends('layouts.portalMaster')


@section('content')
{{--    <div class="page-wrapper">--}}
{{--        <div class="content container-fluid">--}}

{{--            <div class="row">--}}
{{--                <div class="col-lg-12 margin-tb">--}}
{{--                    <div class="pull-left">--}}
{{--                        <h2>Show Member</h2>--}}
{{--                    </div>--}}
{{--                    <div class="pull-right">--}}
{{--                        <a class="btn btn-primary" href="{{ route('admin/policies') }}"> Back</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div><br>--}}

{{--            <div class="row">--}}
{{--                @foreach($member as $mem)--}}
{{--                <div class="col-sm-6">--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-body">--}}
{{--                            <h5 class="card-title">Holder's Information</h5>--}}
{{--                            <table class="table">--}}
{{--                                <thead>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Full Name</th>--}}
{{--                                        <td>{{ $mem->client_firstname . " " . $mem->client_lastname }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Date Of Birth</th>--}}
{{--                                        <td>{{ $mem->client_dob }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Gender</th>--}}
{{--                                        <td>{{ $mem->gender }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">ID Number</th>--}}
{{--                                        <td>{{ $mem->client_id_number }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Address</th>--}}
{{--                                        <td>{{ $mem->strName. ", ". $mem->surburb.", ".$mem->postalCode}}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Home Tel</th>--}}
{{--                                        <td>{{ $mem->home }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Cell</th>--}}
{{--                                        <td>{{ $mem->cell }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Work</th>--}}
{{--                                        <td>{{ $mem->work }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Email</th>--}}
{{--                                        <td>{{ $mem->email }}</td>--}}
{{--                                    </tr>--}}

{{--                                </thead>--}}
{{--                            </table>--}}
{{--                            <a href="" class="btn btn-primary" data-target="#holderModal" data-toggle="modal">Edit</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-6">--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-body">--}}
{{--                            <h5 class="card-title">Spouse's Information</h5>--}}
{{--                            <table class="table">--}}
{{--                                <thead>--}}
{{--                                <tr>--}}
{{--                                    <th scope="row">Full Name</th>--}}
{{--                                    <td>{{ $mem->spouse_firstname . " " . $mem->spouse_lastname }}</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th scope="row">Date Of Birth</th>--}}
{{--                                    <td>{{ $mem->spouse_dob }}</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th scope="row">ID Number</th>--}}
{{--                                    <td>{{ $mem->spouse_idNumber }}</td>--}}
{{--                                </tr>--}}
{{--                                </thead>--}}
{{--                            </table>--}}
{{--                            <a href="" class="btn btn-primary" data-target="#spouseModal" data-toggle="modal">Edit</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="card">--}}
{{--                        <div class="card-body">--}}
{{--                            <h5 class="card-title">Beneficiary's Information</h5>--}}
{{--                            <table class="table">--}}
{{--                                <thead>--}}
{{--                                <tr>--}}
{{--                                    <th scope="row">Full Name</th>--}}
{{--                                    <td>{{ $mem->ben_firstname . " " . $mem->ben_lastname }}</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th scope="row">Date Of Birth</th>--}}
{{--                                    <td>{{ $mem->ben_dob }}</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th scope="row">ID Number</th>--}}
{{--                                    <td>{{ $mem->ben_idNumber }}</td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <th scope="row">Contact Number</th>--}}
{{--                                    <td>{{ $mem->ben_contact_number }}</td>--}}
{{--                                </tr>--}}
{{--                                </thead>--}}
{{--                            </table>--}}
{{--                            <a href="" class="btn btn-primary" data-target="#beneficiaryModal" data-toggle="modal">Edit</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--                <div class="row">--}}
{{--                    <div class="col-sm-6">--}}
{{--                        <div class="card">--}}
{{--                            <div class="card-body">--}}
{{--                                <h5 class="card-title">Package Information</h5>--}}
{{--                                <table class="table">--}}
{{--                                    <thead>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Package</th>--}}
{{--                                        <td>{{ $mem->package }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Status</th>--}}
{{--                                        <td>{{ $mem->status  }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Age Group</th>--}}
{{--                                        <td>{{ $mem->ageGroup. ", ". $mem->marital}}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Amount Cover</th>--}}
{{--                                        <td>{{ $mem->coverAmount}}</td>--}}
{{--                                    </tr>--}}
{{--                                    </thead>--}}
{{--                                </table>--}}
{{--                                <a href="" class="btn btn-primary" data-target="#packageModal" data-toggle="modal">Edit</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="col-sm-6">--}}
{{--                        <div class="card">--}}
{{--                            <div class="card-body">--}}
{{--                                <h5 class="card-title">Dependent's Information</h5>--}}
{{--                                <table class="table">--}}
{{--                                    <thead>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Full Name</th>--}}
{{--                                        <td>{{ $mem->dep_fname . " " . $mem->dep_lname }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Branch</th>--}}
{{--                                        <td>{{ $mem->branchName  }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Date Of Birth</th>--}}
{{--                                        <td>{{ $mem->dep_dob }}</td>--}}
{{--                                    </tr>--}}
{{--                                    </thead>--}}
{{--                                </table>--}}
{{--                                <a href="" class="btn btn-primary" data-target="#dependentModal" data-toggle="modal">Edit</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="row">--}}
{{--                    <div class="col-sm-6">--}}
{{--                        <div class="card">--}}
{{--                            <div class="card-body">--}}
{{--                                <h5 class="card-title">Bank Information</h5>--}}
{{--                                <table class="table">--}}
{{--                                    <thead>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Bank</th>--}}
{{--                                        <td>{{ $mem->bankName  }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Branch</th>--}}
{{--                                        <td>{{ $mem->branchName  }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Branch Code</th>--}}
{{--                                        <td>{{ $mem->branchCode  }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Account Number</th>--}}
{{--                                        <td>{{ $mem->accNumber  }}</td>--}}
{{--                                    </tr>--}}
{{--                                    <tr>--}}
{{--                                        <th scope="row">Account Type</th>--}}
{{--                                        <td>{{ $mem->accType  }}</td>--}}
{{--                                    </tr>--}}
{{--                                    </thead>--}}
{{--                                </table>--}}
{{--                                <a href="" class="btn btn-primary" data-target="#bankModal" data-toggle="modal">Edit</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}


{{--            @endforeach--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    @foreach($member as $mem);--}}
{{--    --}}{{-- holder modal Form --}}
{{--    <div id="holderModal" class="modal fade" data-backdrop="static" tabindex="-1">--}}
{{--        <div class="modal-dialog">--}}
{{--            <div class="modal-content">--}}

{{--                <div class="modal-header"style="background-color: #0C0457;color:white">--}}
{{--                    <h5 class="modal-title">Policy Holder Details</h5><br>--}}
{{--                    <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}

{{--                    <form id="regForm" method="POST" action="{{ route('admin-mail')}}"  enctype="multipart/form-data" >--}}
{{--                    @CSRF--}}
{{--                    <!-- One "tab" for each step in the form: -->--}}
{{--                        <div class="tab">--}}
{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="fname">FirstName</label>--}}
{{--                                    <input id="firstName" class="form-control " type="text" value="{{ $mem->client_firstname }}" name="fname"  required   placeholder=" Firstname">--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="lname">LastName</label>--}}
{{--                                    <input id="lastName" class="form-control " type="text" value="{{ $mem->client_lastname }}" name="lname"  required   placeholder=" Lastname">--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="dob1">D.O.B</label>--}}
{{--                                    <input id="dob1" name="dob1" type="text" class="form-control" required value="{{ $mem->client_dob }}"  >--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="memID">ID. Number</label>--}}
{{--                                    <input id="memID" name="memID" class="form-control " type="text" value="{{ $mem->client_id_number }}"  required   placeholder="ID. Number">--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="row">--}}
{{--                                <div class="custom-file">--}}
{{--                                    <input name="idCopy" type="file" class="form-control" id="customFile" value="{{ $mem->client_idCopy }}" placeholder="Copy of ID">--}}
{{--                                </div>--}}
{{--                            </div><br>--}}

{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="strName">Street Name</label>--}}
{{--                                    <input id="strName" name="strName" class="form-control" type="text" value="{{ $mem->strName }}" required   placeholder="Street Name">--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="surburb">Suburb</label>--}}
{{--                                    <input id="surburb" name="surburb" class="form-control " type="text" value="{{ $mem->surburb}}"   required   placeholder="Surburb">--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="pcode">Postal Code</label>--}}
{{--                                    <input id="pcode" name="pcode" class="form-control " type="text" value="{{ $mem->postalCode }}" required   placeholder="Postal Code">--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="home">Home Tel</label>--}}
{{--                                    <input id="home" name="homeContact" class="form-control" value="{{ $mem->home }}" placeholder="Home: (000) 000 0000" required  >--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="work">Work Tel</label>--}}
{{--                                    <input id="work" name="workContact" class="form-control " type="text" value="{{ $mem->work }}" required   placeholder="Work: (000) 000 0000">--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="cell">Cell</label>--}}
{{--                                    <input id="cell" name="cellContact" class="form-control" value="{{ $mem->cell }}" placeholder="cell: (000) 000 0000" required   >--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="email">Email</label>--}}
{{--                                    <input id="email" name="emailContact" placeholder="example@gmail.com" required value="{{ $mem->email}}"  class="form-control">--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="email"><strong>Gender:</strong></label><br>--}}
{{--                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="genderRadio" id="gridRadios1" value="Male" checked>&nbsp;<span>Male</span>&nbsp;&nbsp;&nbsp;--}}
{{--                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="genderRadio" id="gridRadios2" value="Female">&nbsp;<span>Female</span>--}}

{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <button class="btn btn-primary" type="submit">Update</button>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    --}}{{-- spouse modal Form --}}
{{--    <div id="spouseModal" class="modal fade" data-backdrop="static" tabindex="-1">--}}
{{--        <div class="modal-dialog">--}}
{{--            <div class="modal-content">--}}

{{--                <div class="modal-header"style="background-color: #0C0457;color:white">--}}
{{--                    <h5 class="modal-title">Spouse Details</h5><br>--}}
{{--                    <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}

{{--                    <form class="needs-validation" method="POST" action="{{ url('search-tutorials') }}" enctype="multipart/form-data">--}}
{{--                        @csrf--}}
{{--                        <div class="tab">--}}
{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="sfname">FirstName</label>--}}
{{--                                    <input id="sfname" name="sfname" type="text" class="form-control" required value="{{ $mem->spouse_firstname}}"  placeholder="Firstname">--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="slname">LastName</label>--}}
{{--                                    <input id="slname" name="slname" class="form-control " type="text" value="{{ $mem->spouse_lastname}}"  required   placeholder="Lastname">--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="sid">ID. Number</label>--}}
{{--                                    <input id="sId" name="sId" type="text" class="form-control" required value="{{ $mem->spouse_idNumber}}"  placeholder="ID. Number">--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="sdob">D.O.B</label>--}}
{{--                                    <input id="sdob" name="sdob" class="form-control " type="text" value="{{ $mem->spouse_dob}}"  required  >--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <button class="btn btn-primary" type="submit">Update</button>--}}
{{--                    </form>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    --}}{{-- benefiaciary modal Form --}}
{{--    <div id="beneficiaryModal" class="modal fade" data-backdrop="static" tabindex="-1">--}}
{{--        <div class="modal-dialog">--}}
{{--            <div class="modal-content">--}}

{{--                <div class="modal-header"style="background-color: #0C0457;color:white">--}}
{{--                    <h5 class="modal-title">Beneficiary Details</h5><br>--}}
{{--                    <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}

{{--                    <form class="needs-validation" method="POST" action="{{ url('search-tutorials') }}" enctype="multipart/form-data">--}}
{{--                        @csrf--}}
{{--                        <div class="tab">--}}
{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="benfname">FirstName</label>--}}
{{--                                    <input id="benfname" name="benfname" type="text" class="form-control" required value="{{ $mem->ben_firstname}}"  placeholder="Firstname">--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="benlname">LastName</label>--}}
{{--                                    <input id="benlname" name="benlname" class="form-control " type="text" value="{{ $mem->ben_lastname}}"  required   placeholder="Lastname">--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="benId">ID. Number</label>--}}
{{--                                    <input id="benId" name="benId" type="text" class="form-control" required value="{{ $mem->ben_idNumber}}"  placeholder="ID. Number">--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="bdob">D.O.B</label>--}}
{{--                                    <input id="bdob" name="bdob" class="form-control " type="text"  value="{{ $mem->spouse_dob}}" required  >--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="benconNumber">Contact number</label>--}}
{{--                                    <input id="benconNumber" name="benconNumber" required  value="{{ $mem->ben_contact_number}}" placeholder="Call: (000) 000 0000" class="form-control">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <button class="btn btn-primary" type="submit">Update</button>--}}
{{--                    </form>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    --}}{{-- bank modal Form --}}
{{--    <div id="bankModal" class="modal fade" data-backdrop="static" tabindex="-1">--}}
{{--        <div class="modal-dialog">--}}
{{--            <div class="modal-content">--}}

{{--                <div class="modal-header"style="background-color: #0C0457;color:white">--}}
{{--                    <h5 class="modal-title">Bank Details</h5><br>--}}
{{--                    <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}

{{--                    <form class="needs-validation" method="POST" action="{{ url('search-tutorials') }}" enctype="multipart/form-data">--}}
{{--                        @csrf--}}
{{--                        <div class="tab">--}}
{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="bankName">Bank</label>--}}
{{--                                    <input id="bankName" name="bankName" type="text" class="form-control" required value="{{ $mem->bankName}}"  placeholder="Name of bank">--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="branchName">Bank</label>--}}
{{--                                    <input id="branchName" name="branchName" class="form-control " type="text" required value="{{ $mem->branchName}}"   required   placeholder="Name of branch">--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="row">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="accNumber">Bank</label>--}}
{{--                                    <input id="accNumber" name="accNumber" type="text" class="form-control" required required value="{{ $mem->accNumber}}"  placeholder="Account number">--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="branchCode">Bank</label>--}}
{{--                                    <input id="branchCode" name="branchCode" class="form-control " type="text"   required  required value="{{ $mem->branchCode}}" placeholder="Branch code--}}
{{--">--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="row" style="text-align: left">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="email"><strong>Account type:</strong></label><br>--}}
{{--                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType" id="accType1" value="Current" checked>&nbsp;<span>Current (cheque)</span><br>--}}
{{--                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType" id="accType2" value="Savings">&nbsp;<span>Savings</span><br>--}}
{{--                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="accType" id="accType3" value="Transmission">&nbsp;<span>Transmission</span>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <button class="btn btn-primary" type="submit">Update</button>--}}
{{--                    </form>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    --}}{{-- Package modal Form --}}
{{--    <div id="packageModal" class="modal fade" data-backdrop="static" tabindex="-1">--}}
{{--        <div class="modal-dialog">--}}
{{--            <div class="modal-content">--}}

{{--                <div class="modal-header"style="background-color: #0C0457;color:white">--}}
{{--                    <h5 class="modal-title">Package Details</h5><br>--}}
{{--                    <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}

{{--                    <form class="needs-validation" method="POST" action="{{ url('search-tutorials') }}" enctype="multipart/form-data">--}}
{{--                        @csrf--}}
{{--                        <div class="tab"><h6 id="modal_body"></h6><br>--}}
{{--                            <div class="row" style="text-align: left">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <input id="modal_body1" class="form-control " type="hidden"  name="package">--}}
{{--                                    <select name="ageGroup" class="form-control" required>--}}
{{--                                        <option>Select Age Group</option>--}}
{{--                                        <option value="14-64 yrs">14-64 yrs</option>--}}
{{--                                        <option value="65-74 yrs">65-74 yrs</option>--}}
{{--                                        <option value="75-85 yrs">75-85  yrs</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <label for="email"><strong>Status:</strong></label><br>--}}
{{--                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="marital" id="marital1" value="Single" checked>&nbsp;<span>Single</span><br>--}}
{{--                                    &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="marital" id="marital2" value="Married">&nbsp;<span>Married</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="row" style="text-align: left">--}}
{{--                                <div class="form-group col">--}}
{{--                                    <select name="coverAmount" class="form-control" required>--}}
{{--                                        <option>Select Cover Amount</option>--}}
{{--                                        <option value="5000">R 5 000</option>--}}
{{--                                        <option value="7000">R 7 000</option>--}}
{{--                                        <option value="10000">R 10 000</option>--}}
{{--                                        <option value="13000">R 13 000</option>--}}
{{--                                    </select>--}}

{{--                                </div>--}}
{{--                                <div class="form-group col">--}}
{{--                                    <input id="active" class="form-control " type="text"  name="status">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <button class="btn btn-primary" type="submit">Update</button>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    --}}{{-- Deoendent modal Form --}}
{{--    <div id="dependentModal" class="modal fade" data-backdrop="static" tabindex="-1">--}}
{{--        <div class="modal-dialog">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header"style="background-color: #0C0457;color:white">--}}
{{--                    <h5 class="modal-title">Dependants Details</h5><br>--}}
{{--                    <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}

{{--                    <form class="needs-validation" method="POST" action="{{ url('search-tutorials') }}" enctype="multipart/form-data">--}}
{{--                        @csrf--}}
{{--                        <div class="tab">--}}
{{--                            <div class="inc">--}}
{{--                                <div class="row" style="text-align: left">--}}
{{--                                    <div class="col">--}}
{{--                                        <label>Last Name</label><br>--}}
{{--                                        <input  name="dep_fname" class="form-control">--}}
{{--                                    </div>--}}
{{--                                    <div class="col">--}}
{{--                                        <label>First Name</label><br>--}}
{{--                                        <input name="dep_lname"  class="form-control">--}}
{{--                                    </div>--}}
{{--                                </div><br>--}}

{{--                                <div class="row" style="text-align: left">--}}
{{--                                    <div class="col">--}}
{{--                                        <label>Date Of Birth</label><br>--}}
{{--                                        <input  type="date" name="dep_dob" class="form-control">--}}
{{--                                    </div>--}}
{{--                                    <div class="col">--}}
{{--                                        <label>Birth Certificate Copy</label><br>--}}
{{--                                        <input  type="file" name="depIdCody" id="depIdCody"  class="form-control">--}}
{{--                                    </div>--}}
{{--                                </div><br>--}}

{{--                                <hr>--}}
{{--                            </div>--}}
{{--                            <div class="row" style="text-align: right">--}}
{{--                                <div class="col">--}}
{{--                                    <button  class="btn btn-info" type="submit" id="append" name="append"><i class="fa fa-plus" aria-hidden="true"></i>  Dependent</button>--}}
{{--                                </div>--}}
{{--                            </div><br>--}}
{{--                        </div>--}}
{{--                        <button class="btn btn-primary" type="submit">Update</button>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            </div>--}}
{{--    </div>--}}
{{--                @endforeach;--}}

@endsection
