<footer class="footer-area">
    <div class="container">
        <div class="subscribe-area">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12">
                    <div class="subscribe-content">
                        <h2>CALL ME BACK</h2>
                        <p>Complete the form below and one of our friendly consultants will call you back for a quote.</p>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12">
                    <div class="subscribe-form">
                        <form class="newsletter-form" data-toggle="validator">
                            <input type="email" class="input-newsletter" placeholder="Enter your phone number" name="EMAIL" required autocomplete="off">
                            <button type="submit">CALL ME BACK <i class="flaticon-right-chevron"></i></button>
                            <div id="validator-newsletter" class="form-result"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <div class="logo">
                        <a href="#"><img src="{{asset('public/frontend/img/log.png')}}" alt="image"></a>
{{--                        <p>Atlantic Funeral Service and Burial Fund are an Authorised Financial Services Provider--}}
{{--                            in terms of section 8 of the financial advisory and intermediary services act,--}}
{{--                            2002 (Act No. 37 of 2002) subject to the conditions and restrictions set out in--}}
{{--                            the Annexure. Atlantic Funeral Service has been in existence since 1st August 2001--}}
{{--                            and has over 20 years experience in the funeral industry.--}}

{{--                        </p>--}}
                    </div>
                    <ul class="social">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h3>Quick Links</h3>
                    <ul class="footer-quick-links">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><a href="{{url('/')}}">Our Team</a></li>
                        <li><a href="{{url('/')}}">About Us</a></li>
                        <li><a href="{{url('/')}}">Our Packages</a></li>
                        <li><a href="{{url('/login')}}">My Policy</a></li>
                        <li><a href="{{url('/')}}">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-sm-3 offset-md-3">
                <div class="single-footer-widget">
                    <h3>Contact Info</h3>
                    <ul class="footer-contact-info">
                        <li><span>Location:</span> 16 Soldaat Plein, Factreton, Cape Town</li>
                        <li><span>Email:</span> <a href="#"><span class="__cf_email__" data-cfemail="29594c485b46694e44484045074a4644">info@atlanticfunerals.co.za</span></a></li>
                        <li><span>Phone:</span> <a href="#">+(27) 844478850</a></li>
                      </ul>
                </div>
            </div>
        </div>
        <div class="copyright-area">
            <div class="row align-items-center">
                <div class="col-lg-8 col-sm-8 col-md-8">
                    <p>Copyright ©<script>document.write(new Date().getFullYear());</script>  Atlantic Funerals. All Rights Reserved. Designed by Shifttech Global Solutions</p>

                </div>
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <ul>
                        <li><a href="contact.html">Privacy Policy</a></li>
                        <li><a href="contact.html">Terms & Conditions</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
