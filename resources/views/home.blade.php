@extends('layouts.portalMaster')

@section('content')

<div class="page-wrapper">
    <div class="content container-fluid">

        <div class="page-header">
            <div class="row">
                <div class="col-12">
                    <h3 class="page-title">Welcome   {{ Auth::user()->name }}</h3>
                </div>
            </div>
        </div>
        @if(Auth::user()->hasRole('Admin'))

        <div class="row">
            <div class="col-xl-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="far fa-user"></i>
                            </span>
                            <div class="dash-widget-info">

                                <h3>{{ $all_policies_count }}</h3>

                                <h6 class="text-muted">Policies</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="fas fa-user-shield"></i>
                            </span>
                            <div class="dash-widget-info">
                                <h3>{{ $claims }}</h3>
                                <h6 class="text-muted">Claims</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="far fa-credit-card"></i>
                            </span>
                            <div class="dash-widget-info">
                                <h3>1</h3>
                                <h6 class="text-muted">Agents</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 d-flex">

                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">Recent Policies</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-center">
                                <thead>
                                <tr>

                                    <th>FullName</th>
                                    <th>Phone</th>
{{--                                    <th>Email</th>--}}
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($recent_policies as $policies)
                                    <tr>
                                        <td  hidden>{{ $policies->id}}</td>
                                        <td>
                                            <a href="service-details.html">
                                                <img class="rounded service-img mr-1" src="assets/img/services/service-01.jpg" alt=""> {{ $policies->client_firstname . " " . $policies->client_lastname }}
                                            </a>
                                        </td>
                                        {{--                                        <td>{{ $policy_holder->strName }}</td>--}}
                                        <td>{{ $policies->cell}}</td>
{{--                                        <td>{{ $policies->email }}</td>--}}

                                        <td>
                                            <div class="status-toggle">
                                                <input id="service_1" class="check" type="checkbox" >
                                                <label for="service_1" class="checktoggle">checkbox</label>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/show/'.$policies->policy_no) }}" class="btn btn-info">
                                                <i class="far fa-eye mr-1"></i> View
                                            </a>
                                            {{--                                            <a href="{{ url('admin/edit/'.$policy_holder->policy_no) }}" class="btn btn-primary">--}}
                                            {{--                                                <i class="far fa-edit mr-1"></i> Edit--}}
                                            {{--                                            </a>--}}
                                            <a href="{{ url('admin/delete/'.$policies->policy_no) }}" class="btn btn-danger">
                                                <i class="far fa-trash-alt"></i> Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6 d-flex">

                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">Recent Claims</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-center">
                                <thead>
                                <tr>

{{--                                    <th>Policy Holder</th>--}}
                                    <th>Policy Holder Name</th>
{{--                                    <th>Policy Holder Email</th>--}}
{{--                                    <th>Amount</th>--}}
{{--                                    <th>Date</th>--}}
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($recent_claims as $claims)
                                    <tr>
{{--                                        <td> {{ $claims->policyHolderId }}</td>--}}
                                        <td>
                                            {{ $claims->policyHolderFullName }}
                                        </td>
{{--                                        <td> {{ $claims->policyHolderEmail }}</td>--}}
{{--                                        <td> 5000</td>--}}
{{--                                        <td>{{ $claims->created_at }}</td>--}}

                                        <td>
                                            <div class="status-toggle">
                                                <input id="service_1" class="check" type="checkbox">
                                                <label for="service_1" class="checktoggle">checkbox</label>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="service-details.html" class="btn btn-sm bg-info-light">
                                                <i class="far fa-eye mr-1"></i> View
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        @endif

        @if(Auth::user()->hasRole('Agent'))
            <div class="row">
                <div class="col-xl-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="far fa-user"></i>
                            </span>
                                <div class="dash-widget-info">
                                    <h3>2</h3>
                                    <h6 class="text-muted">Policies</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="fas fa-user-shield"></i>
                            </span>
                                <div class="dash-widget-info">
                                    <h3>2</h3>
                                    <h6 class="text-muted">Tutorials</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="far fa-credit-card"></i>
                            </span>
                                <div class="dash-widget-info">
                                    <h3>R11378</h3>
                                    <h6 class="text-muted">Commission Earned</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 d-flex">

                    <div class="card card-table flex-fill">
                        <div class="card-header">
                            <h4 class="card-title">Recent Policies</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-center">
                                    <thead>
                                    <tr>

                                        <th>Date</th>
                                        <th>Claim</th>
                                        <th>Status</th>
                                        <th>Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>

                                        <td class="text-nowrap">9 Sep 2021</td>
                                        <td>Funeral Cover</td>
                                        <td>
                                            <span class="badge bg-success inv-badge">Approved</span>
                                        </td>
                                        <td>
                                            <div class="font-weight-600">R15000</div>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td class="text-nowrap">9 Oct 2021</td>
                                        <td>Tombstone Cover</td>
                                        <td>
                                            <span class="badge bg-success inv-badge">Approved</span>
                                        </td>
                                        <td>
                                            <div class="font-weight-600">R5000</div>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        @endif

        @if(Auth::user()->hasRole('Client'))
            <div class="row">

                <div class="col-xl-6 col-sm-6 col-12">
                    <div class="card">
                        <a href="{{url('clients/beneficiaries')}}">
                        <div class="card-body">
                            <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="fas fa-users"></i>
                            </span>
                                <div class="dash-widget-info">
                                    <h3>{{$count_beneficiaries }}</h3>
                                    <h6 class="text-muted">Beneficiaries</h6>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>

                <div class="col-xl-6 col-sm-6 col-12">
                    <div class="card">
                        <a href="{{url('clients/payments')}}">
                        <div class="card-body">
                            <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="far fa-credit-card"></i>
                            </span>
                                <div class="dash-widget-info">
                                    <h3>R11378</h3>
                                    <h6 class="text-muted">Premiums</h6>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 d-flex">

                    <div class="card flex-fill">
                        <div class="card-header">
                            <h4 class="card-title">Policy Profile</h4>
                        </div>
                        @foreach ($policy_details as $user)
                        <div class="card-body">
                            <div class="dash-widget-header">
                            <span class="dash-widget-icon bg-primary">
                            <i class="fa fa-user-shield"></i>
                            </span>
                                <div class="dash-widget-info">
                                    <h3> {{ Auth::user()->name }}</h3>
                                    <h6 class="text badge bg-danger inv-badge">Status: {{ $user->status }} </h6>
                                </div>


                            </div><br>
                            <div class="row ">
                                <div class="col">
                                    <h6 class="text">Policy No: {{ $user->policy_no }}</h6>
                                    <h6 class="text">ID No: {{ $user->client_id_number }}</h6>
                                    <h6 class="text">Cell No: {{ $user->cell }}</h6>


                                </div>
                                <div class="col">
                                    <h6 class="text">Package: {{ $user->package }} </h6>
                                    <h6 class="text">Principle: R20000.00</h6>
                                    <h6 class="text">Premium: R150.00</h6>


                                </div>



                            </div>
                            <a href="{{url('clients/policy')}}" class="btn btn-primary btn-block" >View Policy</a>
                        </div>
                        @endforeach
                    </div>

                </div>
                <div class="col-md-6 d-flex">

                    <div class="card card-table flex-fill">
                        <div class="card-header">
                            <h4 class="card-title">My Premiums</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-center">
                                    <thead>
                                    <tr>

                                        <th>Date</th>
                                        <th>Package</th>
                                        <th>Status</th>
                                        <th>Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>

                                        <td class="text-nowrap">9 Sep 2021</td>
                                        <td>Funeral Cover</td>
                                        <td>
                                            <span class="badge bg-success inv-badge">Approved</span>
                                        </td>
                                        <td>
                                            <div class="font-weight-600">R15000</div>
                                        </td>
                                    </tr>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        @endif
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addVid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0C0457;color:white">
                <h5 class="modal-title" id="exampleModalLabel">Add New Video</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="background-color: #ffffff">
                <form method="post" action="{{ route('admin/add-video') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" id="title" name="title" placeholder="Video Title" style="background-color: #F5F5F5;color: black">
                        </div>

                        <div class="form-group">
                            <input type="file" class="form-control" id="file" name="file"  style="background-color: #F5F5F5;color: black">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="description" id="description" placeholder="Video Description"></textarea>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
