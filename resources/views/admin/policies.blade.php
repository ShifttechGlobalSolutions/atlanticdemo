@extends('layouts.portalMaster')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Policies List</h3>
                    </div>
                    <div class="col-auto text-right">
                        <a class="btn btn-light" href="{{ url('clear-search') }}">
                            <i class="fa fa-times">&nbsp;&nbsp;Clear Filter</i>
                        </a>
                        <a class="btn btn-white filter-btn" href="javascript:void(0);" id="filter_search">
                            <i class="fas fa-filter"></i>
                        </a>
                    </div>
                </div>
            </div>


            <div class="card filter-card" id="filter_inputs">
                <div class="card-body pb-0">
                    <form method="POST" action="{{ route('policy-filter')}}"  enctype="multipart/form-data"/>
                    @CSRF
                        <div class="row filter-row">
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label class="col-form-label">From Date</label>
{{--                                    <div class="cal-icon">--}}
                                        <input class="form-control datetimepicker" name="startDate" type="date">
{{--                                    </div>--}}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label class="col-form-label">To Date</label>
{{--                                    <div class="cal-icon">--}}
                                        <input class="form-control datetimepicker" name="endDate" type="date">
{{--                                    </div>--}}
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">

                                <div class="form-group">
                                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>





                </div>
            </div>

            <ul class="nav nav-tabs menu-tabs">
                <li class="nav-item ">
                    <a class="nav-link" data-toggle="tab" href="#all_polices">All Policies <span class="badge badge-primary">{{ $policies }}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#active_polices">Active Policies <span class="badge badge-primary">{{ $active_policies }}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#dormant_polices">Dormant Policies <span class="badge badge-primary">{{ $dormant_policies }}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#cancelled_polices">Cancelled Policies <span class="badge badge-primary"></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#rejected_polices">Rejected Policies <span class="badge badge-primary"></span></a>
                </li>

            </ul>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content profile-tab-cont">
                            <div class="table-responsive tab-pane show active" id="all_polices">
                                <table class="table table-hover table-center mb-0 datatable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>FullName</th>
{{--                                        <th>Location</th>--}}
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Policy</th>
                                        <th>Status</th>
                                        <th style="text-align:center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($policy_holders as $policy_holder)
                                    <tr>
                                        <td>{{ $policy_holder->id}}</td>
                                        <td>
                                            <a href="service-details.html">
                                                <img class="rounded service-img mr-1" src="assets/img/services/service-01.jpg" alt=""> {{ $policy_holder->client_firstname . " " . $policy_holder->client_lastname }}
                                            </a>
                                        </td>
{{--                                        <td>{{ $policy_holder->strName }}</td>--}}
                                        <td>{{ $policy_holder->cell}}</td>
                                        <td>{{ $policy_holder->email }}</td>
                                        <td>{{ $policy_holder->package }}</td>
                                        <td>
                                            <div class="status-toggle">
                                                <input id="service_1" class="check" type="checkbox" checked>
                                                <label for="service_1" class="checktoggle">checkbox</label>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/show/'.$policy_holder->policy_no) }}" class="btn btn-info">
                                                <i class="far fa-eye mr-1"></i> View
                                            </a>
{{--                                            <a href="{{ url('admin/edit/'.$policy_holder->policy_no) }}" class="btn btn-primary">--}}
{{--                                                <i class="far fa-edit mr-1"></i> Edit--}}
{{--                                            </a>--}}
                                            <a href="{{ url('admin/delete/'.$policy_holder->policy_no) }}" class="btn btn-danger">
                                                <i class="far fa-trash-alt"></i> Delete
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    {!! $policy_holders->links() !!}
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive tab-pane fade" id="active_polices">
                                <table class="table table-hover table-center mb-0 datatable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Fullname</th>
{{--                                        <th>Location</th>--}}
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Policy</th>
                                        <th>Status</th>
                                        <th style="text-align:center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($active_policies1 as $Active_policies1)
                                    <tr>
                                        <td>{{ $Active_policies1->id }}</td>
                                        <td>
                                            <a href="service-details.html">
                                                <img class="rounded service-img mr-1" src="assets/img/services/service-01.jpg" alt=""> {{ $Active_policies1->client_firstname . " " . $Active_policies1->client_lastname }}
                                            </a>
                                        </td>
{{--                                        <td>{{ $Active_policies1->strName }}</td>--}}
                                        <td>{{ $Active_policies1->cell }}</td>
                                        <td>{{ $Active_policies1->email }}</td>
                                        <td>{{ $Active_policies1->package }}</td>
                                        <td>
                                            <div class="status-toggle">
                                                <input id="service_1" class="check" type="checkbox">
                                                <label for="service_1" class="checktoggle">checkbox</label>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/show/'.$Active_policies1->policy_no) }}" class="btn btn-info">
                                                <i class="far fa-eye mr-1"></i> View
                                            </a>
                                            <a href="{{ url('admin/delete/'.$Active_policies1->policy_no) }}" class="btn btn-danger">
                                                <i class="far fa-trash-alt"></i> Delete
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    {!! $active_policies1->links() !!}
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive tab-pane fade" id="dormant_polices">
                                <table class="table table-hover table-center mb-0 datatable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Fullname</th>
{{--                                        <th>Location</th>--}}
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Policy</th>
                                        <th>Status</th>
                                        <th style="text-align:center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dormant_policies1 as $Dormant_policies1)
                                    <tr>
                                        <td>{{ $Dormant_policies1->id }}</td>
                                        <td>
                                            <a href="service-details.html">
                                                <img class="rounded service-img mr-1" src="assets/img/services/service-01.jpg" alt=""> {{ $Dormant_policies1->client_firstname . " " . $Dormant_policies1->client_lastname }}
                                            </a>
                                        </td>
{{--                                        <td>{{ $Dormant_policies1->strName }}</td>--}}
                                        <td>{{ $Dormant_policies1->cell }}</td>
                                        <td>{{ $Dormant_policies1->email }}</td>
                                        <td>{{ $Dormant_policies1->package }}</td>
                                        <td>
                                            <div class="status-toggle">
                                                <input id="service_1" class="check" type="checkbox">
                                                <label for="service_1" class="checktoggle">checkbox</label>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/show/'.$Dormant_policies1->policy_no) }}" class="btn btn-info">
                                                <i class="far fa-eye mr-1"></i> View
                                            </a>
                                            <a href="{{ url('admin/delete/'.$Dormant_policies1->policy_no) }}" class="btn btn-danger">
                                                <i class="far fa-trash-alt"></i> Delete
                                            </a>
                                        </td>
                                    </tr>
                                  @endforeach
                                    {!! $active_policies1->links() !!}
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive tab-pane fade" id="cancelled_polices">
                                <table class="table table-hover table-center mb-0 datatable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Fullname</th>
                                        {{--                                        <th>Location</th>--}}
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Policy</th>
                                        <th>Status</th>
                                        <th style="text-align:center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($active_policies1 as $Active_policies1)
                                        <tr>
                                            <td>{{ $Active_policies1->id }}</td>
                                            <td>
                                                <a href="service-details.html">
                                                    <img class="rounded service-img mr-1" src="assets/img/services/service-01.jpg" alt=""> {{ $Active_policies1->client_firstname . " " . $Active_policies1->client_lastname }}
                                                </a>
                                            </td>
                                            {{--                                        <td>{{ $Active_policies1->strName }}</td>--}}
                                            <td>{{ $Active_policies1->cell }}</td>
                                            <td>{{ $Active_policies1->email }}</td>
                                            <td>{{ $Active_policies1->package }}</td>
                                            <td>
                                                <div class="status-toggle">
                                                    <input id="service_1" class="check" type="checkbox">
                                                    <label for="service_1" class="checktoggle">checkbox</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ url('admin/show/'.$Active_policies1->policy_no) }}" class="btn btn-info">
                                                    <i class="far fa-eye mr-1"></i> View
                                                </a>
                                                <a href="{{ url('admin/delete/'.$Active_policies1->policy_no) }}" class="btn btn-danger">
                                                    <i class="far fa-trash-alt"></i> Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    {!! $active_policies1->links() !!}
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive tab-pane fade" id="rejected_polices">
                                <table class="table table-hover table-center mb-0 datatable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Fullname</th>
                                        {{--                                        <th>Location</th>--}}
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Policy</th>
                                        <th>Status</th>
                                        <th style="text-align:center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($active_policies1 as $Active_policies1)
                                        <tr>
                                            <td>{{ $Active_policies1->id }}</td>
                                            <td>
                                                <a href="service-details.html">
                                                    <img class="rounded service-img mr-1" src="assets/img/services/service-01.jpg" alt=""> {{ $Active_policies1->client_firstname . " " . $Active_policies1->client_lastname }}
                                                </a>
                                            </td>
                                            {{--                                        <td>{{ $Active_policies1->strName }}</td>--}}
                                            <td>{{ $Active_policies1->cell }}</td>
                                            <td>{{ $Active_policies1->email }}</td>
                                            <td>{{ $Active_policies1->package }}</td>
                                            <td>
                                                <div class="status-toggle">
                                                    <input id="service_1" class="check" type="checkbox">
                                                    <label for="service_1" class="checktoggle">checkbox</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ url('admin/show/'.$Active_policies1->policy_no) }}" class="btn btn-info">
                                                    <i class="far fa-eye mr-1"></i> View
                                                </a>
                                                <a href="{{ url('admin/delete/'.$Active_policies1->policy_no) }}" class="btn btn-danger">
                                                    <i class="far fa-trash-alt"></i> Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    {!! $active_policies1->links() !!}
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
