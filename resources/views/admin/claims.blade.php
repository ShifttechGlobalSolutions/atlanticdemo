@extends('layouts.portalMaster')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Claims List</h3>
                    </div>
                    <div class="col-auto text-right">
                        <a class="btn btn-white filter-btn" href="javascript:void(0);" id="filter_search">
                            <i class="fas fa-filter"></i>
                        </a>
                    </div>
                </div>
            </div>


            <div class="card filter-card" id="filter_inputs">
                <div class="card-body pb-0">
                    <form>
                        <div class="row filter-row">
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label class="col-form-label">From Date</label>
                                    <div class="cal-icon">
                                        <input class="form-control datetimepicker" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label class="col-form-label">To Date</label>
                                    <div class="cal-icon">
                                        <input class="form-control datetimepicker" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <ul class="nav nav-tabs menu-tabs">
                <li class="nav-item active">
                    <a class="nav-link" data-toggle="tab" href="#all_claims">All Claims <span class="badge badge-primary">1</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#approved_claims">Approved Claims <span class="badge badge-primary">1</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#pending_claims">Pending Claims <span class="badge badge-primary">1</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#cancelled_claims">Cancelled Claims <span class="badge badge-primary">1</span></a>
                </li>


            </ul>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content profile-tab-cont">
                                <div class="table-responsive tab-pane show active" id="all_claims">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>

                                            <th>Policy Holder</th>
                                            <th>Policy Holder Name</th>
                                            <th>Policy Holder Email</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($allclaims as $claims)
                                        <tr>
                                            <td> {{ $claims->policyHolderId }}</td>
                                            <td>
                                                {{ $claims->policyHolderFullName }}
                                            </td>
                                            <td> {{ $claims->policyHolderEmail }}</td>
                                            <td> 5000</td>
                                            <td>{{ $claims->created_at }}</td>

                                            <td>
                                                <div class="status-toggle">
                                                    <input id="service_1" class="check" type="checkbox">
                                                    <label for="service_1" class="checktoggle">checkbox</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="service-details.html" class="btn btn-sm bg-info-light">
                                                    <i class="far fa-eye mr-1"></i> View
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>

                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
