<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AgentPoliciesController extends Controller
{
    //
    public function index()
    {
        return view('agents/policy');
    }
}
