<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\HolderInfo;
use App\Models\SpouseInfo;
use App\Models\BeneficiaryInfo;
use App\Models\BankAccountInfo;
use App\Models\DependentInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class ApplicationController extends Controller
{
    //
    public function index()
    {
        return view('application.applications');
    }

    public function application()
    {
        return view('application.stepper_application');
    }

    public function adminMail(Request $request){
       //dd($request);
        $image = $request->file('idCopy');
        $data = [
            'memberFirstName'=>$request->fname,
            'memberLastName'=>$request->lname,
            'memberDOB'=>$request->dob1,
            'memberIDNum'=>$request->memID,
            'idCopy'=>$image,
            'memberStrName'=>$request->strName,
            'memberSurburb'=>$request->surburb,
            'memberPcode'=>$request->pcode,
            'memberHomeContact'=>$request->homeContact,
            'memberWorkContact'=>$request->workContact,
            'memberCellContact'=>$request->cellContact,
            'memberEmailContact'=>$request->emailContact,
            'memberGenderRadio'=>$request->genderRadio,
            'spouseFname'=>$request->sfname,
            'spouseLname'=>$request->slname,
            'spouseId'=>$request->sId,
            'spouseDOB'=>$request->sdob,
            'dep_fname'=>$request->dep_fname,
            'dep_lname'=>$request->dep_lname,
            'dep_dob'=>$request->dep_dob,
            'depCopiesofid'=>$request->copiesofid,
            'benFname'=>$request->benfname,
            'benLname'=>$request->benlname,
            'benConNumber'=>$request->benconNumber,
            'bankName'=>$request->bankName,
            'branchName'=>$request->branchName,
            'accNumber'=>$request->accNumber,
            'branchCode'=>$request->branchCode,
            'accType'=>$request->accType,
        ];
        $to_Client = $data['memberEmailContact'];
        $to_email = "ricgomo67@gmail.com";
        Mail::to($to_email)->send(new \App\Mail\adminMail($data));
        Mail::to($to_Client)->send(new \App\Mail\clientMail($data));

        if ($request->hasFile('idCopy')) {
            $count = DB::table('holder_infos')->select('id')->count();
            $num = strval( $count );
            $newNum = 1000 + $num;
            $policy_No = 'AT' . $newNum;

            $request->validate([
                'image' => 'mimes:jpeg,bmp,png,jpg,pdf' // Only allow .jpg, .bmp and .png file types.
            ]);

            // Save the file locally in the storage/public/ folder under a new folder named /productImage
            $request->idCopy->store('public/images/members');
            // $request->copiesofid->store('public/images/members');

            $holderInfo = new HolderInfo([
                "policy_no"=>$policy_No,
                "client_firstname"=>$request->get('fname'),
                "client_firstname"=>$request->get('fname'),
                "client_lastname"=>$request->get('lname'),
                "client_dob"=>$request->get('dob1'),
                "client_id_number"=>$request->get('memID'),
                "client_idCopy" => $request->idCopy->hashName(),
                "strName"=>$request->get('strName'),
                "surburb"=>$request->get('surburb'),
                "postalCode"=>$request->get('pcode'),
                "home"=>$request->get('homeContact'),
                "work"=>$request->get('workContact'),
                "cell"=>$request->get('cellContact'),
                "email"=>$request->get('emailContact'),
                "gender"=>$request->get('genderRadio'),
                "premium"=>$request->get('premium'),
                'status'=>'InActive',
                "premium"=>$request->get('premium'),
                "package"=>$request->get('package'),
                "ageGroup"=>$request->get('ageGroup'),
                "marital"=>$request->get('marital'),
                "coverAmount"=>$request->get('coverAmount')

            ]);

            //Spouse
            $spouseInfo = new SpouseInfo([
                "policy_no"=>$policy_No,
                "spouse_firstname"=>$request->get('sfname'),
                "spouse_lastname"=>$request->get('slname'),
                "spouse_idNumber"=>$request->get('sId'),
                "spouse_dob"=>$request->get('sdob'),
            ]);

            $request->depIdCody->store('public/images/dependents');
            $dependentInfo = new DependentInfo([
                "policy_no"=>$policy_No,
                "dep_fname"=>$request->get('dep_fname'),
                "dep_lname"=>$request->get('dep_lname'),
                "dep_dob"=>$request->get('dep_dob'),
                "dep_copiesofid" => $request->depIdCody->hashName(),
            ]);

            $beneficiaryInfo = new BeneficiaryInfo([
                "policy_no"=>$policy_No,
                "ben_firstname"=>$request->get('benfname'),
                "ben_lastname"=>$request->get('benlname'),
                "ben_idNumber"=>$request->get('benId'),
                "ben_dob"=>$request->get('bdob'),
                "ben_contact_number"=>$request->get('benconNumber'),
            ]);

            $bankAccountInfo = new BankAccountInfo([
                "policy_no"=>$policy_No,
                "bankName"=>$request->get('bankName'),
                "branchName"=>$request->get('branchName'),
                "accNumber"=>$request->get('accNumber'),
                "branchCode"=>$request->get('branchCode'),
                "accType"=>$request->get('accType'),
            ]);

            // dd($clientDependent);
            // dd($clientSpouse);
            // dd($ClientPersonalInfo);
            $dependentInfo->save();
            $beneficiaryInfo->save();
            $bankAccountInfo->save();
            $spouseInfo->save();
            $holderInfo->save(); // Finally, save the record.
        }
        return redirect()->back()->with('flash_success', 'Your document has been uploaded.');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'policyHolderEmail' => 'required|policyHolderEmail|unique:users,policyHolderEmail',
            'password' => 'required|same:confirm-password',
            'roles' => 'required',

            'policyHolderName' => 'required',
            'policyHolderSurname' => 'required',
            'policyHolderIdNumber' => 'required',
            'policyHolderAddress' => 'required',
            'policyHolderContact' => 'required',
            'policyHolderGender' => 'required',

            'policyHolderSpouseName' => 'required',
            'policyHolderSpouseSurname' => 'required',
            'policyHolderSpouseIdNumber' => 'required',
            'policyHolderSpouseDateOfBirth' => 'required',
            'policyHolderSpouseGender' => 'required',

            'policyHolderDependantName' => 'required',
            'policyHolderDependantSurname' => 'required',
            'policyHolderDependantIdNumber' => 'required',
            'policyHolderDependantDateOfBirth' => 'required',
            'policyHolderBeneficiaryName' => 'required',
            'policyHolderBeneficiarySurname' => 'required',
            'policyHolderBeneficiaryIdNumber' => 'required',
            'policyHolderBeneficiaryContact' => 'required',
            'policyType' => 'required',
            'policyNumber' => 'required',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
            ->with('success','User created successfully');
    }


}
