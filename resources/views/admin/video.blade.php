@extends('layouts.portalMaster')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">

            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">Video Tutorials</h3>
                    </div>
                    <div class="col-auto text-right">
                        <a class="btn btn-light" href="{{ url('clear-search') }}">
                            <i class="fa fa-times">&nbsp;&nbsp;Clear Filter</i>
                        </a>
                        <a class="btn btn-white filter-btn" href="javascript:void(0);" id="filter_search">
                            <i class="fas fa-filter"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="card filter-card" id="filter_inputs">
                <div class="card-body pb-0">
                    <form method="POST" action=""  enctype="multipart/form-data"/>
                    @CSRF
                    <div class="row filter-row">
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group">
                                <label class="col-form-label">From Date</label>
                                {{--                                    <div class="cal-icon">--}}
                                <input class="form-control datetimepicker" name="startDate" type="date">
                                {{--                                    </div>--}}
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group">
                                <label class="col-form-label">To Date</label>
                                {{--                                    <div class="cal-icon">--}}
                                <input class="form-control datetimepicker" name="endDate" type="date">
                                {{--                                    </div>--}}
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">

                            <div class="form-group">
                                <button class="btn btn-primary btn-block" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            {!! $video->links() !!}
                            <div class="tab-content profile-tab-cont">
                                <div class="table-responsive tab-pane show active" id="all_polices">
                                    <table class="table table-hover table-center mb-0 datatable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Video</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th style="text-align:center">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                @foreach($video as $v)
                                    <tr>
                                        <td>{{ $v->id}}</td>
                                        <td><video  width="120" height="40" controlsList="nodownload">
                                                <source src="{{ asset('public/storage/video/'.$v->video) }}" type="video/mp4">
                                            </video>
                                        </td>

                                        <td>{{ $v->title }}</td>
                                        <td>{{ $v->description }}</td>
                                        <td>
                                            <a href="{{ url('admin/show-video/'.$v->id) }}" class="btn btn-info">
                                                <i class="far fa-eye mr-1"></i> View
                                            </a>
                                            <a href="{{ url('admin/delete-video/'.$v->id) }}" class="btn btn-danger">
                                                <i class="far fa-trash-alt"></i> Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div><br>
                            {!! $video->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
