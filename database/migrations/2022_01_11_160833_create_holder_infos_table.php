<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHolderInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holder_infos', function (Blueprint $table) {
            $table->id();
            $table->string('policy_no');
            $table->string('client_firstname');
            $table->string('client_lastname');
            $table->datetime('client_dob');
            $table->string('client_id_number');
            $table->text('client_idCopy');
            $table->string('strName');
            $table->string('surburb');
            $table->string('postalCode');
            $table->string('home');
            $table->string('work');
            $table->string('cell');
            $table->string('email');
            $table->string('gender');
            $table->string('status');
            $table->string('package');
            $table->string('ageGroup');
            $table->string('marital');
            $table->string('coverAmount');
//            $table->string('premium');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('holder_infos');
    }
}
