<!doctype html>
<html lang="zxx">
<head>


    <link rel="stylesheet" href="{{asset('public/portal/assets/css/admin.css')}}">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="{{asset('public/frontend/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/animate.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/fontawesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/flaticon.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/owl.carousel.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/slick.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/meanmenu.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/magnific-popup.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/odometer.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/nice-select.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('public/frontend/css/responsive.css')}}">
    <title>Atlantic Funerals - Funeral Policy Company</title>
    <link rel="icon" type="image/png" href="{{asset('public/frontend/img/atlantic_logo.png')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>



    <style>

        .modal-login {
            color: #636363;
            width: 650px;
        }
        .modal-login .modal-content {
            padding: 20px;
            border-radius: 5px;
            border: none;
        }
        .modal-login .modal-header {
            border-bottom: none;
            position: relative;
            justify-content: center;
        }
        .modal-login h4 {
            text-align: center;
            font-size: 26px;
            margin: 30px 0 -15px;
        }
        .modal-login .form-control:focus {
            border-color: #70c5c0;
        }
        .modal-login .form-control, .modal-login .btn {
            min-height: 40px;
            border-radius: 3px;
        }
        .modal-login .close {
            position: absolute;
            top: -5px;
            right: -5px;
        }
        .modal-login .modal-footer {
            background: #ecf0f1;
            border-color: #dee4e7;
            text-align: center;
            justify-content: center;
            margin: 0 -20px -20px;
            border-radius: 5px;
            font-size: 13px;
        }
        .modal-login .modal-footer a {
            color: #999;
        }
        .modal-login .avatar {
            position: absolute;
            margin: 0 auto;
            left: 0;
            right: 0;
            top: -70px;
            width: 95px;
            height: 95px;
            border-radius: 50%;
            z-index: 9;
            background: #60c7c1;
            padding: 15px;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
        }
        .modal-login .avatar img {
            width: 100%;
        }
        .modal-login.modal-dialog {
            margin-top: 80px;
        }
        .modal-login .btn, .modal-login .btn:active {
            color: #fff;
            border-radius: 4px;
            background: #60c7c1 !important;
            text-decoration: none;
            transition: all 0.4s;
            line-height: normal;
            border: none;
        }
        .modal-login .btn:hover, .modal-login .btn:focus {
            background: #45aba6 !important;
            outline: none;
        }
        .trigger-btn {
            display: inline-block;
            margin: 100px auto;
        }

        #regForm {
            background-color: #ffffff;
            margin: 100px auto;
            font-family: Raleway;
            padding: 40px;
            width: 70%;
            min-width: 300px;
        }
        h1 {
            text-align: center;
        }
        input {
            padding: 10px;
            width: 100%;
            font-size: 17px;
            font-family: Raleway;
            border: 1px solid #aaaaaa;
        }
        /* Mark input boxes that gets an error on validation: */
        input.invalid {
            background-color: #ffdddd;
        }
        /* Hide all steps by default: */
        .tab {
            display: none;
        }
        button {
            background-color: #04AA6D;
            color: #ffffff;
            border: none;
            padding: 10px 20px;
            font-size: 17px;
            font-family: Raleway;
            cursor: pointer;
        }
        button:hover {
            opacity: 0.8;
        }
        #prevBtn {
            background-color: #bbbbbb;
        }
        /* Make circles that indicate the steps of the form: */
        .step {
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbbbbb;
            border: none;
            border-radius: 50%;
            display: inline-block;
            opacity: 0.5;
        }
        .step.active {
            opacity: 1;
        }
        /* Mark the steps that are finished and valid: */
        .step.finish {
            background-color: #04AA6D;
        }
    </style>
</head>

<body>



@include("patials.header")


@yield('content')


@include("patials.footer")


<div class="go-top"><i class="fas fa-chevron-up"></i><i class="fas fa-chevron-up"></i></div>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="{{asset('public/frontend/js/jquery.min.js')}}"></script>

<script src="{{asset('public/frontend/js/popper.min.js')}}"></script>

<script src="{{asset('public/frontend/js/bootstrap.min.js')}}"></script>

<script src="{{asset('public/frontend/js/parallax.min.js')}}"></script>

<script src="{{asset('public/frontend/js/owl.carousel.min.js')}}"></script>

<script src="{{asset('public/frontend/js/slick.min.js')}}"></script>

<script src="{{asset('public/frontend/js/jquery.meanmenu.js')}}"></script>

<script src="{{asset('public/frontend/js/jquery.appear.min.js')}}"></script>

<script src="{{asset('public/frontend/js/odometer.min.js')}}"></script>

<script src="{{asset('public/frontend/js/jquery.nice-select.min.js')}}"></script>

<script src="{{asset('public/frontend/js/jquery.magnific-popup.min.js')}}"></script>

<script src="{{asset('public/frontend/js/wow.min.js')}}"></script>

<script src="{{asset('public/frontend/js/jquery.ajaxchimp.min.js')}}"></script>

<script src="{{asset('public/frontend/js/form-validator.min.js')}}"></script>

<script src="{{asset('public/frontend/js/contact.blade.php-form-script.js')}}"></script>

<script src="{{asset('public/frontend/js/main.js')}}"></script>

{{----}}
{{--<script src="{{asset('public/portal/assets/js/jquery-3.5.0.min.js')}}"></script>--}}

{{--<script src="{{asset('public/portal/assets/js/popper.min.js')}}"></script>--}}
{{--<script src="{{asset('public/portal/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>--}}

{{--<script src="{{asset('public/portal/assets/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>--}}

{{--<script src="{{asset('public/portal/assets/js/admin.js')}}"></script>--}}



<script>
    jQuery(document).ready( function () {
        $("#append").click( function(e) {
            e.preventDefault();
            $(".inc").append('<div class="controls">\
                <div class="row" style="text-align: left">\
                <div class="col">\
                <label>Last Name</label><br>\
            <input name="depfname" class="form-control">\
            </div>\
            <div class="col">\
                <label>First Name</label><br>\
                <input name="deplname"  class="form-control">\
            </div>\
        </div><br>\
        <div class="row" style="text-align: left">\
                    <div class="col">\
                        <label>Date Of Birth</label><br>\
                        <input  type="date" id="dep1dob"  class="form-control">\
                    </div>\
                    <div class="col">\
                        <label>Birth Certificate Copy</label><br>\
                        <input  type="file"  class="form-control">\
                    </div>\
                </div><br>\
                <hr>\
            </div>');
            return false;
        });

        jQuery(document).on('click', '.remove_this', function() {
            jQuery(this).parent().remove();
            return false;
        });
        $("input[type=submit]").click(function(e) {
            e.preventDefault();
            $(this).next("[name=textbox]")
                .val(
                    $.map($(".inc :text"), function(el) {
                        return el.value
                    }).join(",\n")
                )
        })
    });

    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab
    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Submit";
        } else {
            document.getElementById("nextBtn").innerHTML = "Next";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }
    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }
    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }
    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }
</script>

</body>


</html>
