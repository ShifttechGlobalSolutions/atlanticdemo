<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AdminProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }
    public function index()
    {
       $logs= Auth::user()->email;
        $users = DB::table('users')
           ->where('email',$logs )->get();
       // dd($users);
        return view('admin/profile', ['users' => $users]);
        //return view('admin/profile');
    }
}
